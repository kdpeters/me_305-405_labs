
"""
@file main.py

@brief This file runs the FSMs that generate arrays of encoder data based on user input
"""
import Encoder
from DataGen import DataGenerator
import pyb
from DataUI import UserInterface

## Encoder object for encoder finite state machine
encoder = Encoder.EncoderDriver(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3)

## DataGenerator finite state machine
task1 = DataGenerator(encoder)

## UI finite state machine
task2 = UserInterface()

while True:
    task1.run()
    task2.run()
    
    
