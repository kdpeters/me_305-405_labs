'''
@file Shared.py
@brief Script to define and store global variables to be shared between FSMs
'''
## Current user input command
cmd = 0

## Time array for data collection
tArray = None

## Data array for data collection
dArray = None

## New data available for sending
newd = False
