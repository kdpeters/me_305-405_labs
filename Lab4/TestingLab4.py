
"""
@file TestingLab4.py

@brief This file tests serial communication
"""
import serial

ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)

def sendchar():
    inp = input('Give me a character: ')
    ser.write(str(inp).encode('ascii'))
    val = ser.readline().decode('ascii')
    return val

for n in range(3):
    print(sendchar())

ser.close()

