"""
@file FrontofUI.py

@brief This file serves as the user interface for data collection on the encoder
@details Uses the serial module to send user input to the Nucleo and encoder data 
         back to the user.\n'G' starts data collection, 'S' stops data collection
"""
import serial
import matplotlib.pyplot as plt
import time
import csv

## Serial communication setup
ser = serial.Serial(port='COM3', baudrate=115273, timeout=2)

print('Commands at your disposal:\n')
print('\'G\' - Start data collection for ten senconds')
print('\'S\' - Stop data collection prematurely and get results')
print('Note: If you spend the full 10s collecting data, you will need to enter')
print('\'G\' again once 10s have elapsed to receive the data')

## Starting time for data collection
starttim = 0

## Initialization of the encoder data
datain = [['0'], ['0']]

def recplot():
    '''
    @brief Takes in user input for data collection commands and retrieves data from the Nucleo
    @details Once 'G' is entered, data is retrieved after 10 seconds or when 'S'
             is entered
    @return a two-dimensional list whose first row is a list of string objects
            representing the time since the start of data collection in microseconds.
            The second row is a list of string objects representing tick position
            of the encoder at the correspnding times.
    '''
    global starttim
    data = [['0'], ['0']]
    inp = input('Input: ')
    val = str(inp).encode('ascii')
    while((val.decode('ascii') != 'G') and (val.decode('ascii') != 'S')):
        print('Please enter a valid command.')
        inp = input('Input: ')
        val = str(inp).encode('ascii')
    if((val.decode('ascii') == 'G') and (starttim == 0)):
        ser.write(val)
        starttim = time.time()
    elif(val.decode('ascii') == 'S'):
        if(starttim == 0):
            print('Data collection hasn\'t started yet!')
        else:
            ser.write(val)
            starttim = 0
            time.sleep(0.3)
            while(ser.in_waiting > 0):
                rline = ser.readline().decode('ascii')
                rlist = str(rline).strip().split(',')
                data[0].append(rlist[0])
                data[1].append(rlist[1])
            return data
    if((time.time()-starttim >= 10) and (starttim != 0)):
        time.sleep(0.3)
        while(ser.in_waiting > 0):
            rline = ser.readline().decode('ascii')
            rlist = str(rline).strip().split(',')
            data[0].append(rlist[0])
            data[1].append(rlist[1])
    return data

while(datain == [['0'], ['0']]):
    datain = recplot()

ser.close()

for n in range(len(datain[0])):
    datain[0][n] = int(datain[0][n])
    datain[1][n] = int(datain[1][n])
    datain[0][n] = datain[0][n]/(1e6)
    datain[1][n] = (datain[1][n]/(28*50))*(360)

## Creating plot of encoder position over time
plt.plot(datain[0], datain[1])
plt.ylabel('Shaft Position [degrees]')
plt.xlabel('Time [seconds]')
plt.gcf()
plt.savefig('EncoderPosition.png')

## Creating csv file of recorded data
with open('EncoderPosition.csv', 'w', newline='') as f:
    ## Writer object for creating csv file
    dawriter = csv.writer(f)
    dawriter.writerow(['Time [seconds]', 'Shaft Position [degrees]'])
    for n in range(len(datain[0])):
        dawriter.writerow([datain[0][n], datain[1][n]])










