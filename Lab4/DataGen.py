
"""
@file DataGen.py

@brief This file serves as the FSM to collect data from encoder channels
@details Uses the Encoder class to obtain position data on the Nucleo and 
         store it for transmission to the computer
"""

import utime
import Shared
import array


class DataGenerator:
    '''
    @brief      A finite state machine to run data generation from encoder channels
    
    @details    This class implements a finite state machine to record data during the 
                time period specified by the user. Collection is at 5Hz for 10 seconds 
                by default but stops short if the user halts collection early
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Data Acquisition
    S1_datacq = 1
       
    def __init__(self, encoder):
        '''
        @brief      Creates a DataGenerator object
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
           
        ## Running task time interval in microseconds
        self.interval = int(0.2*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Time array for data collection
        self.Tarray = None
        
        ## Data array for data collection
        self.Darray = None
        
        ## Class attribute Encoder object
        self.enc = encoder
        
        ## Data collection start time
        self.datastart = None
    
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                # Run State 0 Code
                if(Shared.cmd == 71):
                    self.Darray = array.array('l', [])
                    self.Tarray = array.array('l', [])
                    self.datastart = utime.ticks_us()
                    self.enc.set_position(0)
                    self.transitionTo(self.S1_datacq)
            
            elif(self.state == self.S1_datacq):
                # Run State 1 Code
                telapsed = utime.ticks_diff(utime.ticks_us(), self.datastart)
                if((Shared.cmd != 83) and (telapsed < 10*(1e6))):
                    self.enc.update()
                    self.Darray.append(self.enc.get_position())
                    self.Tarray.append(telapsed)
                    self.transitionTo(self.S1_datacq)
                else:
                    Shared.dArray = self.Darray
                    Shared.tArray = self.Tarray
                    Shared.newd = True
                    Shared.cmd = 0
                    self.transitionTo(self.S0_init)

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

