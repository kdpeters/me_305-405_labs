
"""
@file DataUI.py

@brief This file serves as the user interface for data collection run on the Nucleo
@details Uses the UART module to take user input to the Nucleo return encoder data 
         back to the computer
"""

import utime
import Shared
from pyb import UART
import array



class UserInterface:
    '''
    @brief      A finite state machine to run a basic user interface with a data collection FSM
    
    @details    This class implements a finite state machine to read user input
                data to control a data collection object. It sends finished data
                series back to the computer
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Checking and taking action
    S1_check = 1
       
    def __init__(self):
        '''
        @brief      Creates a UserInterface object
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Class attribute uart object
        self.uint = UART(2)
        
        ## Running task time interval in microseconds
        self.interval = int(0.05*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                # Run State 0 Code
                self.transitionTo(self.S1_check)
            
            elif(self.state == self.S1_check):
                if(self.uint.any() > 0):
                    Shared.cmd = self.uint.readchar()
                elif(Shared.newd):
                    for n in range(len(Shared.dArray)):
                        data = array.array('l', [])
                        data.append(Shared.tArray[n])
                        data.append(Shared.dArray[n])
                        self.uint.write('{:},{:}'.format(data[0], data[1]) +'\r\n')
                    Shared.newd = False
                    Shared.cmd = 0
                self.transitionTo(self.S1_check)

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState