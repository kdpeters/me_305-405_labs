
"""
@file ButtonUI.py
@brief This file is a computer front end for button pin voltage data acquisition from the Nucleo.
@details The user sends a "ready" command to the Nucleo then presses the user button.
         Sent Nucleo data is received and processed into a .csv file and plot of
         button pin voltage vs time. The RC circuit's time constant is also determined.
"""
import serial
import matplotlib.pyplot as plt
import time
import csv
from matplotlib.ticker import MultipleLocator
import numpy as np

## Serial communication setup
ser = serial.Serial(port='COM3', baudrate=115273, timeout=2)

## Array of data from the Nucleo; time and voltage data
data = [[], []]

# Ensuring proper program use
print('Enter \'G\' and then press the button to record voltage data.')
print('Be sure reset the Nucleo between data samplings and rerunning this program.')
inp = input('Input: ')
while(inp!='G'):
    inp = input('Input: ')
inp = str(inp).encode('ascii')
ser.write(inp)

while (ser.in_waiting==0):
    pass
time.sleep(1)
while(ser.in_waiting > 0):
    ## Line read from serial port translated back into a string
    rline = ser.readline().decode('ascii')
    ## Array created by separating line at the comma
    rrline = rline.strip().split(',')
    # adding a time data point
    data[0].append(float(rrline[0]))
    # adding a voltage data point
    data[1].append(float(rrline[1]))

# scaling voltage
for g in range(len(data[1])):
    data[1][g] = data[1][g]/1240.91
    
# creating .csv file
with open('ButtonPinVolt.csv', 'w', newline='') as csvfile:
    ## CSV writer
    vwriter = csv.writer(csvfile)
    for n in range(len(data[0])):
        vwriter.writerow([data[0][n], data[1][n]])

# plotting voltage against time
## Figure
fig, ax = plt.subplots()
ax.plot(data[0], data[1])
ax.set_xlabel('Time [milliseconds]')
ax.set_ylabel('Digital Button Pin Voltage [volts]')
ax.axis([0, 2.5, 0, 3.3])
ax.yaxis.set_major_locator(MultipleLocator(0.3))
plt.show()
fig.savefig('ButtVolts.png')

## DC voltage associated a high pin value
vdd = 3.3
## Negative natural log of (Vdd - button pin voltage)
adjustedv = []
## Time in seconds
adjustedt = []
for g in range(len(data[1])):
    adjustedv.append(-1*np.log(vdd-data[1][g]))
    adjustedt.append(data[0][g]/1000)
## Index of data where a linear fit stops being accurate
stopidx = 0
for h in range(len(adjustedt)):
    if adjustedt[h]>0.0013:
        stopidx = h
        break
## Time data within linear range
adjustedtt = np.zeros(h-50)
## Natural log voltage data within linear range
adjustedvv = np.zeros(h-50)
for i in range(len(adjustedtt)):
    adjustedtt[i] = adjustedt[i+50]
    adjustedvv[i] = adjustedv[i+50]

## Linear fit plot
plt.figure(1)
plt.scatter(adjustedtt, adjustedvv)
## m = slope of linear fit; b = intercept of linear fit
(m, b) = np.polyfit(adjustedt, adjustedv, 1)
print('The time constant based on the data is '+'{:.7f}'.format(1/m)+' seconds')

ser.close()











    