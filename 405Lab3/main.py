
"""
@file main.py
@brief This file uses a analog to digital converter module to record button voltage after button release.
@details After receiving a "ready" command from the computer, an array is filled
         with voltage data sampled at a rate determined by an internal timer. Once
         a set of data containing the button pin voltage recovery is captured, 
         time data is created and the two sets of data are sent back to the computer.
"""
import pyb
from pyb import Pin
from pyb import Timer
import array
from pyb import UART

## UART object
uint = UART(2)

## Pin connected to button pin via jumper on the Nucleo
p0 = Pin(Pin.cpu.A0)

## ADC object
adc = pyb.ADC(p0)

## Timer frequency
frequency = 125000

## Timer to control ADC sampling
Tim2 = Timer(2, freq=frequency)

## Data collection array
buffer = array.array('H', (0 for idx in range(6000)))

## Response curve starting data index
startidx = 0

## Response curve ending data index
endidx = 0

## Valid data found boolean
found = False

## User command
cmd = ''

# checking for user command
while True:
    while True:
        if uint.any()>0:
            cmd = uint.readline().decode('ascii').strip()
            if cmd=='G':
                break
    # filling the buffer array and chekcing if valid data is contained within
    while True:
        adc.read_timed(buffer, Tim2)
        if (max(buffer)>4070 and min(buffer)<5):
            for val in buffer:
                if val==4070:
                    found = True
                    break
        if (found):
            break
    # finding desired data within the buffer array
    for n in range(len(buffer)-1):
        if (buffer[n+1]>(buffer[n]+10)):
            startidx = n
            for m in range(len(buffer)-n-1):
                if buffer[m+n+1]>4080:
                    endidx = m+n+1
                    break
            break
    
    ## Data point time stamp
    time = 0
    # sending the data to the computer
    for p in range(endidx-startidx+1):
        uint.write('{:.4f},{:}'.format(time, buffer[startidx+p])+'\r\n')
        time += 4.8/1000



