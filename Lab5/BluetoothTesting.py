
"""
Testing Bluetooth Connection to Nucleo
"""
import pyb
from pyb import UART

pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
uart = UART(3, 9600)

while True:
    if(uart.any()):
        val = int(uart.readline())
        if val == 0:
            print(val, ' turns LED OFF')
            pinA5.low()
        if val == 1:
            print(val, ' turns LED ON')
            pinA5.high()
