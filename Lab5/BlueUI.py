"""
@file BlueUI.py

@brief This file serves as the user interface for LED operation on the Nucleo
@details Uses the UART module built into the Bluetooth driver to take user input 
         to the Nucleo and return messages back to the smartphone. 
"""

import utime
from BlueDrive import BlueDrive

class BlueUser:
    '''
    @brief      A finite state machine to run a basic user interface with a Bluetooth driver
    
    @details    This class implements a finite state machine to read user input
                data to control Nucleo LED flashing. It sends messages back to the 
                smartphone
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Checking/LED OFF
    S1_OFF = 1
    
    ## Constant defining State 2 - Checking/LED ON
    S2_ON = 2
       
       
    def __init__(self, bluedrive):
        '''
        @brief Creates a UserInterface object
        @param bluedrive Bluetooth driver object to interact with the Nucleo
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## class attribute BlueDrive object
        self.bd = bluedrive
        
        ## Running task time interval in microseconds
        self.interval = int(0.01*(1e6))
        
        ## Run counter
        self.runs = 0
        
        ## Number of runs until LED flash
        self.maxrun = 1000000000000
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                self.transitionTo(self.S1_OFF)
            
            elif(self.state == self.S1_OFF):
                self.bd.ledoff()
                if(self.bd.anything()):
                    uinput = int(self.bd.read())
                    self.maxrun = int(100/uinput - 5)
                    self.bd.write('LED blinkin\' at ' + str(uinput) +' Hz')
                    self.runs = 0
                elif(self.runs >= self.maxrun):
                    self.runs = 0
                    self.transitionTo(self.S2_ON)
            
            elif(self.state == self.S2_ON):
                self.bd.ledon()
                if(self.bd.anything()):
                    self.transitionTo(self.S1_OFF)
                elif(self.runs >= 5):
                    self.runs = 0
                    self.transitionTo(self.S1_OFF)
            self.next_time = utime.ticks_add(self.next_time, self.interval) 
            self.runs += 1
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState int representing the next state to transition to
        '''
        self.state = newState

