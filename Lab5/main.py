
"""
@file main.py

@brief This file runs the FSM that manages user input from a smartphone
"""
from BlueDrive import BlueDrive
from BlueUI import BlueUser

## Bluetooth driver object
bd = BlueDrive()

## UI finite state machine
bu = BlueUser(bd)

while True:
    bu.run()