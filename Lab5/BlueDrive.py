"""
@file BlueDrive.py

@brief Bluetooth driver class to operate Nucleo LED from smartphone commands
@details This file constains a class that essentially acts as a wrapper for UART
"""
from pyb import Pin
from pyb import UART

class BlueDrive:
    '''
    @brief driver class that implements the basic uart functionality in addition to LED lighting
    '''
    def __init__(self):
        '''
        @brief Constructs a BlueDrive object

        '''
        ## class attribute UART object
        self.uart = UART(3, 9600)
        
        ## class attribute Pin object connected to Nucleo LED
        self.pinled = Pin(Pin.cpu.A5, Pin.OUT_PP)
        
    def read(self):
        '''
        @brief Extends UART "read" functionality
        @return what is read from one line in the UART input buffer
        '''
        return self.uart.readline()
    
    def write(self, ipt):
        '''
        @brief Extends UART "write" functionality
        @param ipt string representing what is to be sent back to the smartphone
        '''
        self.uart.write(ipt)
    
    def anything(self):
        '''
        @brief Extends UART "any" functionality
        @return number of items waiting in the UART input buffer
        '''
        return self.uart.any()
    
    def ledon(self):
        '''
        @brief Turns nucleo LED on
        '''
        self.pinled.high()

    def ledoff(self):
        '''
        @brief Turns nucleo LED off
        '''
        self.pinled.low()







