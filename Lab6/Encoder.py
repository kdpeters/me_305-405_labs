
"""
@file Encoder.py

@brief This file contains an Encoder object that keeps track of its position

@details The Encoder object incorporates methods that allow for getting and 
         setting position.

"""
import pyb


class EncoderDriver:
    '''
    @brief Represents a magnetic quadrature encoder with a 16-bit counter
    
    @details Keeps track of net motor position since initialization or last reset
             Methods allow for getting and setting position
    '''
    ## Period value for a 16-bit counter
    Period = 0xFFFF
    
    def __init__(self, pinA, pinB, timno):
        '''
        @brief Constructs an Encoder object
        @details Creates a timer and appropriate channels from input parameters
        @param pinA pin connected to encoder channel 1
        @param pinB pin connected to encoder channel 2
        @param timno int representing the timer number corresponding to pins
        '''
        ## Input Pin object for Channel 1
        self.encoderA = pinA
        
        ## Input Pin object for Channel 2
        self.encoderB = pinB
        
        ## Input Timer designation corresponding to Pin inputs
        self.timerno = timno
        
        ## Class Timer object initialization
        self.tim = pyb.Timer(self.timerno, prescaler=0, period=self.Period)
        
        ## Timer Channel 1 initialization
        self.ch1 = self.tim.channel(1, pin=self.encoderA, mode = pyb.Timer.ENC_AB)
        
        ## Timer Channel 2 initialization
        self.ch2 = self.tim.channel(2, pin=self.encoderB, mode = pyb.Timer.ENC_AB)
        
        ## Most recent position obtained from the encoder
        self.pos = 0
        
        ## Position obtained from the encoder previous to the most recent
        self.prepos = 0
        
        ## Net position of the encoder
        self.position = 0
        
        ## Delta position between last two calls to update()
        self.delta = 0
        
    def update(self):
        '''
        @brief Updates the net position of the motor based on encoder readings
        '''
        self.prepos = self.pos
        self.pos = self.tim.counter()
        self.get_delta()
        self.position += self.delta
    
    def get_position(self):
        '''
        @brief Returns the current net position of the motor
        @return int representing net position of motor since initialization/reset
        '''
        return self.position
    
    def set_position(self, posit):
        '''
        @brief Resets the net position of the motor to inputted value
        @param posit int value of the new motor position
        '''
        self.position = posit
        self.pos = 0
        self.prepos = 0
    
    def get_delta(self):
        '''
        @brief Computes the correct difference in position from previous encoder readings
        @return int value of the delta in position
        '''
        diff = self.pos - self.prepos
        if (abs(diff) > 0.5*self.Period):
            if (diff > 0):
                diff -= self.Period
            elif (diff < 0):
                diff += self.Period
        self.delta = diff
        return self.delta












