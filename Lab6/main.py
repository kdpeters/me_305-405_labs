'''
@file main.py
@brief Script to run motor control and motor response data collection
'''

from Encoder import EncoderDriver
from MotorDriver import MotoDrive
import pyb
from MotorUI import MotorUser
from MotorCtrl import Controller
from ClosedLoop import ClosedLoop

## Encoder object for Controller FSM
encoder = EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

## MotoDrive object for Controller FSM
motodrive = MotoDrive(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, pyb.Timer(3, freq=20000))

## ClosedLoop object for Controller FSM
closedloop = ClosedLoop(0.001, 400)

## MotorUser FSM
task1 = MotorUser()

## Controller FSM
task2 = Controller(encoder, motodrive, closedloop)

while True:
    task1.run()
    task2.run()
    

