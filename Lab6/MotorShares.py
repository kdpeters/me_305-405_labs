'''
@file MotorShares.py
@brief Script to define and store global variables to be shared between FSMs
'''
## Current user input gain
kp = 0

## Data array for omega actual collection
dArray = None

## Omega reference for Kp testing
omegaref = 0
