
"""
@file ClosedLoop.py

@brief This file contains a ClosedLoop object that computes motor level

@details Uses proportional control and a provided gain to determine appropriate
         motor effort levels

"""

class ClosedLoop:
    '''
    @brief Computes the motor effort level to control the step function response 
    
    @details Methods include updating motor level as well as getting and setting
             the gain value
    '''
    def __init__(self, kpi, omegaref):
        '''
        @brief Creates a ClosedLoop object
        @param kpi float defining the initial Kp value
        @param omegaref float defining the desired speed of the motor
        '''
        ## Gain value
        self.kp = kpi
        
        ## Reference omega
        self.oref = omegaref
        
        ## Kp limit
        self.kplim = 330/self.oref
        
        if(self.kp > self.kplim):
            self.kp = self.kplim
    
    def update(self, omegacurr):
        '''
        @brief Computes the motor effor level according to the gain and current error
        @param omegacurr float representing the current motor speed
        @return int representing the computed effort level in percent
        '''
        level = (self.kp/3.3)*(self.oref - omegacurr)
        return int(level)
    
    def getKp(self):
        '''
        @brief gets the current gain value
        @return float representing the current gain value
        '''
        return self.kp
    
    def setKp(self, newkp):
        '''
        @brief sets the gain to the new value
        @param newkp float defining the new gain value
        '''
        if(newkp > self.kplim):
            self.kp = self.kplim
        else:
            self.kp = newkp
    
    
    
    