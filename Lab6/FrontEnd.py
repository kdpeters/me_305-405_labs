"""
@file FrontEnd.py

@brief This file serves as the user interface for Kp data collection on motor response
@details Uses the serial module to send user input to the Nucleo and motor data 
         back to the user
"""
import serial
import matplotlib.pyplot as plt
import time

## Serial communication setup
ser = serial.Serial(port='COM3', baudrate=115273, timeout=2)

print('Commands at your disposal:\n')
print('Enter a value for Kp to test. The motor response data for reaching 400 RPM')
print('will be returned.')
      

## Starting time for data collection
starttim = 0

## Initialization of the timing data
times = []
for n in range(500):
    times.append(n/1000)



def testKp():
    '''
    @brief Takes in user input for the system gain and retrieves data from the Nucleo
    @details Once a value for Kp is entered, data is retrieved after 0.5 seconds
             showing the step function motor response targeting 400 RPM
    @return a list representing the speed of the motor in RPM at regular time
            intervals corresponding to those in the "times" list
    '''
    global starttim
    data = []
    inp = input('Input: ')
    val = 0
    while(val == 0):
        try:
            val = float(inp)
        except ValueError:
            print('Please enter a valid command.')
            inp = input('Input: ')
    val = str(inp).encode('ascii')
    ser.write(val)
    starttim = time.time()
    
    time.sleep(4)
    while(ser.in_waiting > 0):
        rline = ser.readline().decode('ascii')
        data.append(rline.strip())
    return data

## Speed profile data collection
datain = testKp()

ser.close()

for n in range(len(datain)):
    datain[n] = float(datain[n])
for n in range(len(datain)):
    if(datain[n] > 500):
        datain[n] = 0.5*(datain[n-1] + datain[n+1])
    
datain[0] = 0


## Creating plot of encoder position over time
plt.plot(times, datain)
plt.ylabel('Shaft Speed [RPM]')
plt.xlabel('Time [seconds]')
plt.gcf()
plt.savefig('SpeedProfile.png')











