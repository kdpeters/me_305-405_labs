
"""
@file MotorCtrl.py

@brief This file serves as the FSM to run a step function motor response
@details Uses the Encoder, MotoDrive, and ClosedLoop classes to obtain angular 
         velocity data on the Nucleo and store it for transmission to the computer
"""

import utime
import MotorShares
import array

class Controller:
    '''
    @brief      A finite state machine to run data collect of motor response
    
    @details    This class implements a finite state machine to record data during 
                a .5-second time period and coordinate the proportional control of 
                the motor
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Data Acquisition
    S1_datacq = 1
       
    def __init__(self, encoder, motodrive, closedloop):
        '''
        @brief Creates a Controller object
        @param encoder EncoderDriver object to get motor position data
        @param motodrive MotoDrive object to interact with the motor hardware
        @param closedloop ClosedLoop object for computing the level to send to the motor
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
           
        ## Running task time interval in microseconds
        self.interval = int(0.001*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Data array for data collection
        self.Darray = None
        
        ## Class attribute EncoderDriver object
        self.enc = encoder
        
        ## Class attribute MotoDrive object
        self.md = motodrive
        
        ## Class attribute ClosedLoop object
        self.clp = closedloop
        
        ## Data collection start time
        self.datastart = None
    
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                if(MotorShares.kp != 0):
                    self.Darray = array.array('f', [])
                    self.datastart = utime.ticks_us()
                    self.enc.set_position(0)
                    self.md.enable()
                    self.md.setDuty(0)
                    self.transitionTo(self.S1_datacq)
            
            elif(self.state == self.S1_datacq):
                telapsed = utime.ticks_diff(utime.ticks_us(), self.datastart)
                if(telapsed < 0.5*(1e6)):
                    self.clp.setKp(MotorShares.kp)
                    self.enc.update()
                    drevs = (self.enc.get_delta()/4000)
                    omegacurr = 60*(drevs/(self.interval/(1e6)))
                    level = self.clp.update(omegacurr)
                    self.md.setDuty(-1*level)
                    self.Darray.append(omegacurr)
                else:
                    MotorShares.dArray = self.Darray
                    self.md.setDuty(0)
                    self.md.disable()
                    self.enc.set_position(0)
                    self.transitionTo(self.S0_init)

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState int representing the next state to transition to
        '''
        self.state = newState

