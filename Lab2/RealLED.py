
"""
@file RealLED.py
@brief This file contains a finite-state-machine for complex LED control.

@details Controls a Nucleo LED that changes brightness according to a set 
         sawtooth pattern.
"""
import utime
import pyb
from pyb import Pin

class RLEDTasker:
    '''
    @brief      A finite state machine to control the patterned blinking of an LED.
    @details    This class implements a finite state machine to control the
                brightness of an LED on a Nucleo microcontroller board according
                to a sawtooth pattern.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Incrementing
    S1_incre = 1
    
    ## Constant defining State 2 - Incrementing
    S2_incre = 2
    
    ## Maximum brightness level
    Mbright = 100
       
    def __init__(self, pin, interval):
        '''
        @brief      Creates an RLEDTasker object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Class attribute "copy" of the Pin object
        self.Pin = pin
        
        ## "Tooth" width time interval in microseconds
        self.interval = int(interval*1e6)
        
        ## Run time interval: 1/100 of the "tooth" time interval
        self.steptime = int(self.interval/100)
         
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## Brightness level
        self.Bright = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.steptime)
        
        ## Timer for running LED signal
        self.timer = pyb.Timer(2, freq = 20000)
        
        ## Channel for controlling LED from timer
        self.channel = self.timer.channel(1, pyb.Timer.PWM, pin=self.Pin)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                # Run State 0 Code
                self.transitionTo(self.S1_incre)
            
            elif(self.state == self.S1_incre):
                # Run State 1 Code
                if(self.Bright < self.Mbright):
                    self.Bright += 1
                    self.channel.pulse_width_percent(self.Bright)
                    self.transitionTo(self.S2_incre)
                elif(self.Bright == self.Mbright):
                    self.Bright = 0
                    self.channel.pulse_width_percent(self.Bright)
                    self.transitionTo(self.S2_incre)

            elif(self.state == self.S2_incre):
                # Run State 2 Code
                if(self.Bright < self.Mbright):
                    self.Bright += 1
                    self.channel.pulse_width_percent(self.Bright)
                    self.transitionTo(self.S1_incre)
                elif(self.Bright == self.Mbright):
                    self.Bright = 0
                    self.channel.pulse_width_percent(self.Bright)
                    self.transitionTo(self.S1_incre)

            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.steptime) 
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

