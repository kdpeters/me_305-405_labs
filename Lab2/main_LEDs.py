'''
@file main_LEDs.py
@brief Script to run LED tasks
'''

from VirtLED import VLEDTasker
from RealLED import RLEDTasker
import pyb
from pyb import Pin


      
## Pin object for Nucleo LED
pinA5 = Pin(Pin.cpu.A5)

## Task object for virtual LED
task1 = VLEDTasker(1)

## Task object for Nucleo LED
task2 = RLEDTasker(pinA5, 10)
# To run the tasks over and over

while True: 
    task1.run()
    task2.run()
#   task3.run()
