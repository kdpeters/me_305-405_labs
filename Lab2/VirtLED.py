"""
@file VirtLED.py
@brief This file contains a finite-state-machine for LED control.

@details Controls a virtual LED that cycles on and off at regular intervals.

"""
import time

class VLEDTasker:
    '''
    @brief      A finite state machine to control the cyclic blinking of a virtual LED.
    @details    This class implements a finite state machine to control the
                printing of "on" and "off" messages to the screen.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - LEDON
    S1_on = 1
    
    ## Constant defining State 2 - LEDOFF
    S2_off = 2
       
    def __init__(self, interval):
        '''
        @brief      Creates an VLEDTasker object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Blinking time interval
        self.interval = interval
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970       
    
        ## The timestamp for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = time.time()    #updating the current timestamp
        
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_init):
                #print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_on)

            
            elif(self.state == self.S1_on):
                #print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                print('LED is ON')
                self.transitionTo(self.S2_off)

            
            elif(self.state == self.S2_off):
                #print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                print('LED is OFF')
                self.transitionTo(self.S1_on)

            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

