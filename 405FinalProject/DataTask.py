"""
@file DataTask.py
@brief This file contains a generator task to manage state variable data collection from the balancing system
"""
import Shares
import pyb
import cotask

def runData():
    '''
    @brief Generator task that facilitates state variable data collection
    @details 
    '''
    tim = 0
    state = 0
    while True:
        #print('Data')
        if state==0:
            if Shares.data.get():
                state = 1
        elif state==1:
            Shares.time.append(tim)
            if Shares.qballx.any():
                Shares.xar.append(Shares.qballx.get())
            if Shares.qbally.any():
                Shares.yar.append(Shares.qbally.get())
            if Shares.qballz.any():
                Shares.zar.append(Shares.qballz.get())
            if Shares.qplatx.any():
                Shares.thxar.append(Shares.qplatx.get())
            if Shares.qplaty.any():
                Shares.thyar.append(Shares.qplaty.get())
            if Shares.qvelx.any():
                Shares.xdotar.append(Shares.qvelx.get())
            if Shares.qvely.any():
                Shares.ydotar.append(Shares.qvely.get())
            if Shares.qomegx.any():
                Shares.omxar.append(Shares.qomegx.get())
            if Shares.qomegy.any():
                Shares.omyar.append(Shares.qomegy.get())
            if Shares.qDC1.any():
                Shares.DC1ar.append(Shares.qDC1.get())
            if Shares.qDC2.any():
                Shares.DC2ar.append(Shares.qDC2.get())
            tim+=25
            if not Shares.data.get():
                state = 0
                tim = 0
        yield(0)

## Task object to be added to scheduler task list
data = cotask.Task (runData, name = 'DataTask', priority = 3, period = 25, profile = True, trace = False)
cotask.task_list.append (data)

