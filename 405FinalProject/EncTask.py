"""
@file EncTask.py
@brief This file contains a generator task to operate the motor encoders
"""
import Shares
import EncDriver
from pyb import Pin
import math
import cotask

## Period of task in ms
Period = 25
def runEncoder():
    '''
    @brief Generator task that facilitates encoder position reading
    @details Takes motor position data from two EncDriver objects. This is then 
             converted from encoder ticks to radians and translated to platform
             angle from motor angle. Angular velocity is then computed. Platform
             data is stored in shared variables.
    '''
    pinb6 = Pin(Pin.cpu.B6, mode=Pin.IN)
    pinb7 = Pin(Pin.cpu.B7, mode=Pin.IN)
    pinc6 = Pin(Pin.cpu.C6, mode=Pin.IN)
    pinc7 = Pin(Pin.cpu.C7, mode=Pin.IN)
    enc1 = EncDriver.EncoderDriver(pinb6, pinb7, 4)
    enc2 = EncDriver.EncoderDriver(pinc6, pinc7, 8)
    preposx = 0
    preposy = 0
    rm = 60
    lp = 110
    dt = Period/1000
    while True:
        #print('Encoder')
        if Shares.encSet.get():
            enc1.set_position(0)
            enc2.set_position(0)
            Shares.encSet.put(False)
        enc1.update()
        enc2.update()
        thetax = enc1.get_position()*(2*math.pi/4000)*(-rm/lp)
        thetay = enc2.get_position()*(2*math.pi/4000)*(-rm/lp)
        omegax = (thetax-preposx)/dt
        omegay = (thetay-preposy)/dt
        preposx = thetax
        preposy = thetay
        Shares.encx.put(thetax)
        Shares.ency.put(thetay)
        Shares.encvelx.put(omegax)
        Shares.encvely.put(omegay)
        yield(0)

## Task object to be added to scheduler task list
encoder = cotask.Task (runEncoder, name = 'EncoderTask', priority = 3, period = Period, profile = True, trace = False)
cotask.task_list.append (encoder)


# Be sure to set encoder position when level

