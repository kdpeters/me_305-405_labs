"""
@file ControllerTask.py
@brief This file contains a generator task to implement a full state feedback closed control loop
"""
import Shares
import cotask

def runCtrl():
    '''
    @brief Generator task that implements closed loop gains to state vector data to produce motor PWM signals
    @details Takes state vector data from various sensors and applies gains
             determined from analytical calculations and testing. Full state
             feedback is implemented for each motor separately so two pulse width
             percantages are calculated: one for each motor to manage either the
             pitch or roll of the platform. 
             
    '''
    state = 0
    ## terminal resistance (ohms) from motor datasheet
    R = 2.21
    ## torque constant (Nm/A) from motor datasheet
    Kt = .0138
    ## DC voltage supplied (V)
    Vdc = 12
    runs = 0
    pcon = 100*R/(4*Kt*Vdc)
    while True:
        #print('Ctrl')
        if state==0:
            if Shares.bal.get():
                state=1
                

        if state==1:
            (x, y, z) = (Shares.ballx.get(), Shares.bally.get(), Shares.ballz.get())
            (xdot, ydot) = (Shares.velx.get(), Shares.vely.get())
            (thx1, thy1) = (Shares.encx.get(), Shares.ency.get())
            (thx2, thy2) = (Shares.imux.get(), Shares.imuy.get())
            (omx1, omy1) = (Shares.encvelx.get(), Shares.encvely.get())
            (omx2, omy2) = (Shares.imuvelx.get(), Shares.imuvely.get())

            if not z:
                # implement some no contact condition
                DC1 = 0
                DC2 = 0
                Xstate = [0,0,0,0]
                Ystate = [0,0,0,0]
            
            # Decide on platform angles/angular velocitites to use
            elif x>.09:
                Xstate = [0,0,0,0]
                Ystate = [0,0,0,0]
                DC1 = 0
                DC2 = 0
            else:
                K = [-1.50, -.75, -.31, -.06]
                Kp = [pcon*K[0], pcon*K[1], pcon*K[2], pcon*K[3]]
                # state vector for controlling motor 2
                Xstate = [x, thy2, xdot, omy2]
    
                # state vector for controlling motor 1
                Ystate = [y, thx2, ydot, omx2]
    
                DC2 = Kp[0]*Xstate[0]+Kp[1]*Xstate[1]+Kp[2]*Xstate[2]+Kp[3]*Xstate[3]
                DC1 = Kp[0]*Ystate[0]+Kp[1]*Ystate[1]+Kp[2]*Ystate[2]+Kp[3]*Ystate[3]
                DC1*=3.0
                DC2*=-4.0
                if abs(DC1)>100:
                    if DC1>0:
                        DC1 = 100
                    else:
                        DC1 = -100
                if abs(DC2)>100:
                    if DC2>0:
                        DC2 = 100
                    else:
                        DC2 = -100
            
            if Shares.data.get():
                Shares.qballx.put(x)
                Shares.qbally.put(y)
                Shares.qballz.put(z)
                Shares.qplatx.put(thx2)
                Shares.qplaty.put(thy2)
                Shares.qvelx.put(xdot)
                Shares.qvely.put(ydot)
                Shares.qomegx.put(omx2)
                Shares.qomegy.put(omy2)
                Shares.qDC1.put(DC1)
                Shares.qDC2.put(DC2)
            
            # if runs%20==0:
            #     print('Xstate: '+str(Xstate))
            #     print('Ystate: '+str(Ystate))
            #     print('DC1: '+str(DC1))
            #     print('DC2: '+str(DC2))
            
            Shares.duty1.put(DC1)
            Shares.duty2.put(DC2)
            if not Shares.bal.get():  
                Shares.duty1.put(0)
                Shares.duty2.put(0)
                state = 0
                    

            runs+=1
        
        yield(0)

## Task object to be added to scheduler task list
controller = cotask.Task (runCtrl, name = 'ControlTask', priority = 4, period = 25, profile = True, trace = False)
cotask.task_list.append (controller)