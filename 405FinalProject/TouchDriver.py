"""
@file TouchDriver.py
@brief This file contains a driver class for a resistive touch panel
"""
from pyb import Pin
from pyb import ADC
import utime

class TouchDriver:
    '''
    @brief Driver class to capture the functionality of the resistive touch panel
    @details Driver methods include an X-position, Y-position, Z-detection, and 
             XYZ position scan.
    '''
    
    def __init__(self, xpPin, xmPin, ypPin, ymPin, length, width, origin):
        '''
        @brief Constructor for TouchDriver object
        @param xpPin pyb.Pin object associated with the positive terminal of the horizontal direction
        @param xmPin pyb.Pin object associated with the negative terminal of the horizontal direction
        @param ypPin pyb.Pin object associated with the positive terminal of the vertical direction
        @param ymPin pyb.Pin object associated with the negative terminal of the vertical direction
        @param length int representing the span of the touch panel in the horizontal direction
        @param width int representing the span of the touch panel in the vertical direction
        @param origin tuple containing the x and y coordinate of the center of the platform
        '''
        ## Horizontal positive pin
        self.xp = xpPin
        ## Horizontal negative pin
        self.xm = xmPin
        ## Vertical positive pin
        self.yp = ypPin
        ## Vertical negative pin
        self.ym = ymPin
        ## Analog to digital converter pin
        self.adc = None
        ## Horizontal span of pad
        self.length = length
        ## Vertical span of pad
        self.width = width
        ## Platform origin coordinates
        self.origin = origin
        ## Smallest horizontal reading acquired from testing
        self.xmin = 206
        ## Largest horizontal reading acquired from testing
        self.xmax = 3795
        ## Horizontal reading range
        self.xspan = self.xmax-self.xmin
        ## Smallest vertical reading acquired from testing
        self.ymin = 517
        ## Largest vertical reading acquired from testing
        self.ymax = 3684
        ## Vertical reading range
        self.yspan = self.ymax-self.ymin
    
    def Xscan(self):
        '''
        @brief Acquires horizontal x position of touch on pad relative to origin
        @return int representing x position in mm. Returns None if no touch is detected.
        '''
        self.xp.init(mode=Pin.OUT_PP)
        self.xm.init(mode=Pin.OUT_PP)
        self.yp.init(mode=Pin.OUT_OD)
        self.ym.init(mode=Pin.OUT)
        self.adc = ADC(self.ym)
        self.xp.high()
        self.yp.high()
        self.xm.low()
        utime.sleep_us(7)
        val = self.adc.read() - self.xmin
        
        if val<0:
            return None
        else:
            edgedist = self.length*val/self.xspan
            centerdist = edgedist-self.origin[0]
            return centerdist
            
    
    def Yscan(self):
        '''
        @brief Acquires vertical y position of touch on pad relative to origin
        @return int representing y position in mm. Returns None if no touch is detected.
        '''
        self.xp.init(mode=Pin.OUT_OD)
        self.ym.init(mode=Pin.OUT_PP)
        self.yp.init(mode=Pin.OUT_PP)
        self.xm.init(mode=Pin.OUT)
        self.adc = ADC(self.xm)
        self.xp.high()
        self.yp.high()
        self.ym.low()
        utime.sleep_us(7)
        val = self.adc.read() - self.ymin
        
        if val<0:
            return None
        else:
            edgedist = self.width*val/self.yspan
            centerdist = edgedist-self.origin[1]
            return centerdist
        
    
    def Zscan(self):
        '''
        @brief Acquires z status of touch on pad (touch detection)
        @return boolean representing whether contact is detected on the touch pad.
        '''
        self.xp.init(mode=Pin.OUT)
        self.ym.init(mode=Pin.OUT)
        self.yp.init(mode=Pin.OUT_PP)
        self.xm.init(mode=Pin.OUT_PP)
        self.adc = ADC(self.ym)
        self.yp.high()
        self.xm.low()
        utime.sleep_us(7)
        val = self.adc.read()
        
        if val < 4080:
            return True
        else:
            return False
    
    def XYZscan(self):
        '''
        @brief Acquires x, y, and z positions of touch on pad in mm
        @return tuple of positional data (x, y, z).
        '''
        self.xp.init(mode=Pin.OUT)
        self.ym.init(mode=Pin.OUT)
        self.yp.init(mode=Pin.OUT_PP)
        self.xm.init(mode=Pin.OUT_PP)
        self.adc = ADC(self.ym)
        self.yp.high()
        self.xm.low()
        utime.sleep_us(7)
        if self.adc.read()<4080:
            positionz = True
        else:
            return (None, None, False)
        self.xp.init(mode=Pin.OUT_PP)
        self.yp.init(mode=Pin.OUT_OD)
        self.xp.high()
        self.yp.high()
        utime.sleep_us(7)
        val = self.adc.read() - self.xmin
        if val<0:
            positionx = None
        else:
            positionx = (self.length*val/self.xspan) - self.origin[0]
        self.xm.init(mode=Pin.OUT)
        self.adc = ADC(self.xm)
        self.xp.init(mode=Pin.OUT_OD)
        self.xp.high()
        self.ym.init(mode=Pin.OUT_PP)
        self.ym.low()
        self.yp.init(mode=Pin.OUT_PP)
        self.yp.high()
        utime.sleep_us(7)
        val = self.adc.read() - self.ymin
        if val<0:
            positiony = None
        else:
            positiony = (self.width*val/self.yspan) - self.origin[1]
        return (positionx, positiony, positionz)


if __name__=='__main__':
    ## Horizontal positive pin
    xp = Pin(Pin.cpu.A7)
    ## Horizontal negative pin
    xm = Pin(Pin.cpu.A1)
    ## Vertical positive pin
    ym = Pin(Pin.cpu.A0)
    ## Vertical negative pin
    yp = Pin(Pin.cpu.A6)
    ## Horizontal span of pad in mm
    length = 176
    ## Vertical span of pad in mm
    width = 100
    ## Coordinates of origin of pad
    origin = (90.0, 49.5, 0)
    ## TouchDriver object
    tch = TouchDriver(xp, xm, yp, ym, length, width, origin)
    utime.sleep(2)
    # Code for testing touch pad readings in mm from center
    for n in range(30):
        print(tch.Xscan())
        utime.sleep(1)
    print('Y time!')
    for n in range(30):
        print(tch.Yscan())
        utime.sleep(1)
    print('Z time!')
    for n in range(35):
        print(tch.Zscan())
        utime.sleep(1)
    ## Start time for testing speed of XYZscan()
    starttime = utime.ticks_us()
    for n in range(60):
        tch.XYZscan()
    ## End time for testing speed of XYZscan()
    endtime = utime.ticks_us()
    print(utime.ticks_diff(endtime, starttime)/60)
    ## List of touch pad coordinates
    coords = []
    for n in range(70):
        coords.append(tch.XYZscan())
        if not coords[-1][2]:
            coords[-1] = (0,0,False)
        utime.sleep(0.25)
    for n in range(30):
        print(tch.XYZscan())
        utime.sleep(1)
    # with open ('TouchRect.csv', 'w') as file:
    #     for n in range(len(coords)):
    #         file.write('{:.4f},{:.4f},{:}\r\n'.format(coords[n][0], coords[n][1], coords[n][2]))
    
    # print('CSV file created')
    
    

    
    






