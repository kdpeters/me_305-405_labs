"""
@file MotTask.py
@brief This file contains a generator task to operate the motor system
"""
import Shares
import MotDriver
import cotask
from pyb import Pin
import pyb

def runMotor():
    '''
    @brief Generator task that facilitates motor motion
    @details Applies PWM duty cycles from shared variables to the two motor channels.
             Shares motor fault status as well and sits idle during a fault until
             user input re-enables the motor system.
    '''
    ## nSleep Pin
    pina15 = Pin(Pin.cpu.A15)
    ## nFault Pin
    pinb2 = Pin(Pin.board.PB2)
    ## Motor 1 IN1
    pinb4 = Pin(Pin.cpu.B4)
    ## Motor 1 IN2
    pinb5 = Pin(Pin.cpu.B5)
    ## Motor 2 IN1
    pinb0 = Pin(Pin.cpu.B0)
    ## Motor 2 IN2
    pinb1 = Pin(Pin.cpu.B1)
    ## Timer for PWM
    timer3 = pyb.Timer(3, freq=20000)
    ## DRV8847 object
    drv = MotDriver.DRV8847(pina15, pinb2)
    ## MotorChannel object for Motor 1
    mot1 = drv.channel(pinb4, pinb5, timer3)
    ## MotorChannel object for Motor 2
    mot2 = drv.channel(pinb0, pinb1, timer3)
    drv.enable()
    ## State of finite state machine
    state = 0
    while True:
        # get duty levels from controller
        if state==0:
            mot1.setDuty(Shares.duty1.get())
            mot2.setDuty(Shares.duty2.get())
            if drv.faulty:
                state = 1
                Shares.fault.put(True)
                print('Motor fault detected.')
        # fault state, wait for clear
        elif state==1:
            if not Shares.fault.get():
                drv.clearFault()
                drv.enable()
                state = 0
        yield(state)

## Task object to be added to scheduler task list
motor = cotask.Task (runMotor, name = 'MotorTask', priority = 4, period = 25, profile = True, trace = False)
cotask.task_list.append (motor)
