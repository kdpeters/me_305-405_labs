
"""
@file TouchTask.py
@brief This file contains a generator task to operate the resistive touch pad
"""
import Shares
import TouchDriver
from pyb import Pin
import cotask

## Period of task in ms
Period = 15
def runTouch():
    '''
    @brief Generator task that facilitates ball position tracking
    @details Takes positional data from a TouchDriver object and computes ball
             velocity. Position and velocity data are then stored in shared
             variables.
    '''
    xp = Pin(Pin.cpu.A7)
    xm = Pin(Pin.cpu.A1)
    ym = Pin(Pin.cpu.A0)
    yp = Pin(Pin.cpu.A6)
    length = 176
    width = 100
    origin = (90.0, 49.5, 0)
    pad = TouchDriver.TouchDriver(xp, xm, yp, ym, length, width, origin)
    preposx = 0
    preposy = 0
    dt = Period/1000
    while True:
        #print('Touch')
        ballposition = pad.XYZscan()
        posx = ballposition[0]
        if posx==None:
            posx = preposx
        posy = ballposition[1]
        if posy==None:
            posy = preposy
        velox = (posx-preposx)/dt
        veloy = (posy-preposy)/dt
        preposx = posx
        preposy = posy
        Shares.ballx.put(posx/1000)
        Shares.bally.put(posy/1000)
        Shares.ballz.put(ballposition[2])
        Shares.velx.put(velox/1000)
        Shares.vely.put(veloy/1000)
        yield(0)

## Task object to be added to scheduler task list
touch = cotask.Task (runTouch, name = 'TouchTask', priority = 3, period = Period, profile = True, trace = False)
cotask.task_list.append (touch)
