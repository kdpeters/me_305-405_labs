"""
@file UITask.py
@brief This file contains a generator task to manage user commands for the balancing system
"""
import Shares
from pyb import USB_VCP
import cotask

def runUI():
    '''
    @brief Generator task that facilitates receiving and sending of user commands to the control task
    @details Takes user commands from the PUTTY REPL and adds them to a shared
             queue for interpretation. The user can start/stop platform balancing,
             handle motor faults, and start/stop data collection.
    '''
    vcp = USB_VCP()
    while True:
        #print('UI')
        if vcp.any():
            cmd = int.from_bytes(vcp.read(), 'big')
            if cmd==122:
                Shares.encSet.put(True)
                print('Zeroed')
            elif cmd==99:
                Shares.fault.put(False)
                print('Fault cleared')
            elif cmd==100:
                Shares.data.put(True)
                print('Data Start')
            elif cmd==101:
                Shares.data.put(False)
                print('Data Stop')
            elif cmd==103:
                Shares.bal.put(True)
                print('Start')
            elif cmd==115:
                Shares.bal.put(False)
                print('Stop')
            cmd=0
                
            
            
        yield(0)

## Task object to be added to scheduler task list
ui = cotask.Task (runUI, name = 'UITask', priority = 2, period = 50, profile = True, trace = False)
cotask.task_list.append (ui)