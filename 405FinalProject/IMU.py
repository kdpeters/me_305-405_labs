
"""
@file IMU.py
@brief This file contains an intertial measurement unit object that tracks orientation
"""
from pyb import I2C
import utime

class BNO055:
    '''
    @brief Wrapper class for IMU functionality of the Adafruit BNO055 sensor
    @details Sensor incorporates data from an internal accelerometer, gyroscope, 
             and magnetometer. Methods include an operation mode set, calibration 
             check, Euler angle acquisition, and angular velocity acquisition.
    '''
    ## Configuration mode byte 
    config_mode = 0x00
    ## Nine degrees of freedom fusion mode byte
    ndof_mode = 0x0c
    ## Calibration status register
    calib_reg = 0x35
    ## Operating mode register
    mode_reg = 0x3d
    
    def __init__(self, i2c, address):
        '''
        @brief Constructor for BNO055 object
        @param i2c Master I2C object for control of the physical IMU sensor
        @param address Physical IMU sensor device address byte
        '''
        ## I2C object class copy
        self.myi2c = i2c
        ## Device address
        self.slavead = address
        ## Read data array from a device memory register
        self.readdata = bytearray([0])
    
    def calcheck(self):
        '''
        @brief Checks if the device is properly calibrated for all sensors
        @return boolean representing device's calibration status.
        '''
        self.myi2c.mem_read(self.readdata, self.slavead, self.calib_reg)
        # need 0b11111111 for full calibration
        cal = self.readdata[0]
        print(bin(cal))
        if cal==0b11111111:
            #print('Calibrated')
            return True
        else:
            return False
    def opMode(self, newmode):
        '''
        @brief Sets the current operating mode for the BNO055 sensor
        @param newmode byte corresponding to the new desired operating mode
        '''
        self.myi2c.mem_write(self.config_mode, self.slavead, self.mode_reg)
        utime.sleep_ms(20)
        if newmode != self.config_mode:
            self.myi2c.mem_write(newmode, self.slavead, self.mode_reg)
            utime.sleep_ms(10)
            
    def getAngles(self):
        '''
        @brief Acquires Euler angles of the platform in radians
        @return tuple containing Euler angles of the platform relative to calibration
                point (yaw, pitch, roll).
        '''
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1b)
        xm = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1a)
        xl = self.readdata[0]
        word = (xm<<8)|xl
        if word>32767:
            word -= 65536
        eulx = word/900
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1d)
        ym = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1c)
        yl = self.readdata[0]
        word = (ym<<8)|yl
        if word>32767:
            word -= 65536
        euly = word/900
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1f)
        zm = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x1e)
        zl = self.readdata[0]
        word = (zm<<8)|zl
        if word>32767:
            word -= 65536
        eulz = word/900
        return (eulx, euly, eulz)
    
    def getOmegas(self):
        '''
        @brief Acquires angular velocities of the platform in rad/s
        @return tuple containing angular velocities about sensor axes (omegax, omegay, omegaz).
        '''
        self.myi2c.mem_read(self.readdata, self.slavead, 0x19)
        xm = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x18)
        xl = self.readdata[0]
        word = (xm<<8)|xl
        if word>32767:
            word -= 65536
        omegax = word/900
        self.myi2c.mem_read(self.readdata, self.slavead, 0x17)
        ym = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x16)
        yl = self.readdata[0]
        word = (ym<<8)|yl
        if word>32767:
            word -= 65536
        omegay = word/900
        self.myi2c.mem_read(self.readdata, self.slavead, 0x15)
        zm = self.readdata[0]
        self.myi2c.mem_read(self.readdata, self.slavead, 0x14)
        zl = self.readdata[0]
        word = (zm<<8)|zl
        if word>32767:
            word -= 65536
        omegaz = word/900
        return (omegax, omegay, omegaz)

if __name__=='__main__':
    ## I2C object
    i2c = I2C(1)
    i2c.init(I2C.MASTER)
    ## BNO055 object
    sensor = BNO055(i2c, 0x28)
    sensor.opMode(0x0c)
    ## List of pitch data
    pitch = []
    
    ## List of pitch rate
    pitchrate = []
    while sensor.calcheck()==False:
        pass
    print(sensor.calcheck())
    for n in range(60):
        angles = sensor.getAngles()
        omegas = sensor.getOmegas()
        print('Pitch: '+str(angles[2]))
        print('PR: '+str((-1*omegas[2])))
        pitch.append(angles[2])
        pitchrate.append(-omegas[2])
        utime.sleep(1)
        
    with open ('PitchData.csv', 'w') as file:
        time = 0
        for n in range(60):
            file.write('{:},{:},{:}\r\n'.format(time, pitch[n], pitchrate[n]))
            time+=1
    print('CSV file created')
        
        
        
        
        
        
        
        