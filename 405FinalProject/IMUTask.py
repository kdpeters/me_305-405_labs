"""
@file IMUTask.py
@brief This file contains a generator task to operate the IMU
"""
import Shares
import IMU
from pyb import I2C
import cotask

def runIMU():
    '''
    @brief Generator task that facilitates data collection from IMU
    @details Takes Euler angle and angular velocity data from a BNO055 object.
             This data is stored in shared variables and supplements platform
             angle data from the encoders.
    '''
    i2c = I2C(1)
    i2c.init(I2C.MASTER)
    ## BNO055 object
    sensor = IMU.BNO055(i2c, 0x28)

    while sensor.calcheck()==False:
        pass
    print('IMU calibration: '+str(sensor.calcheck()))
    while True:
        #print('IMU')
        angles = sensor.getAngles()
        omegas = sensor.getOmegas()
        thetax = -angles[1]
        thetay = angles[2]
        omegax = omegas[1]
        omegay = -omegas[2]
        Shares.imux.put(thetax)
        Shares.imuy.put(thetay)
        Shares.imuvelx.put(omegax)
        Shares.imuvely.put(omegay)
        yield(0)

## Task object to be added to scheduler task list
imu = cotask.Task (runIMU, name = 'IMUTask', priority = 3, period = 25, profile = True, trace = False)
cotask.task_list.append (imu)