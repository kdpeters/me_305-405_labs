
"""
@file Shares.py
@brief This file contains a collection of Share and Queue objects for data sharing across tasks
"""
import task_share
import array
## Boolean for whether the system has a motor current fault 
fault = task_share.Share('B', thread_protect = True, name = 'Motor Fault')

## Boolean for whether the system is collecting data 
data = task_share.Share('B', thread_protect = True, name = 'Data Collect')

## Boolean for whether the system is balancing 
bal = task_share.Share('B', thread_protect = True, name = 'Balancing')

## Ball x position on platform
ballx = task_share.Share('f', thread_protect = True, name = 'Ball X')

## Ball y position on platform
bally = task_share.Share('f', thread_protect = True, name = 'Ball Y')

## Ball z presence on platform
ballz = task_share.Share('B', thread_protect = True, name = 'Ball Z')

## Encoder zeroing requested by user
encSet = task_share.Share('B', thread_protect = True, name = 'Encoder Zeroing')

## Ball x velocity on platform
velx = task_share.Share('f', thread_protect = True, name = 'Ball VeloX')

## Ball y velocity on platform
vely = task_share.Share('f', thread_protect = True, name = 'Ball VeloY')

## Platform angle about x-axis
encx = task_share.Share('f', thread_protect = True, name = 'Platform ThetaX')

## Platform angle about y-axis
ency = task_share.Share('f', thread_protect = True, name = 'Platform ThetaY')

## Platform angular velocity about x-axis 
encvelx = task_share.Share('f', thread_protect = True, name = 'Platform OmegaX')

## Platform angular velocity about y-axis
encvely = task_share.Share('f', thread_protect = True, name = 'Platform OmegaY')

## PWM level for motor 1
duty1 = task_share.Share('f', thread_protect = True, name = 'Motor1 Duty')

## PWM level for motor 2
duty2 = task_share.Share('f', thread_protect = True, name = 'Motor2 Duty')

## IMU platform angle about x-axis
imux = task_share.Share('f', thread_protect = True, name = 'IMU ThetaX')

## IMU platform angle about y-axis
imuy = task_share.Share('f', thread_protect = True, name = 'IMU ThetaY')

## IMU platform angular velocity about x-axis
imuvelx = task_share.Share('f', thread_protect = True, name = 'IMU OmegaX')

## IMU platform angular velocity about y-axis
imuvely = task_share.Share('f', thread_protect = True, name = 'IMU OmegaY')

#usercmd = task_share.Queue('H', 10, thread_protect = True, overwrite = False, name = 'User Command')
## Ball x position data queue
qballx = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Ball X Data')

## Ball y position data queue
qbally = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Ball Y Data')

## Ball z status data queue
qballz = task_share.Queue('B', 100, thread_protect = True, overwrite = False, name = 'Ball Z Data')

## Ball y velocity data queue
qvely = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Ball Ydot Data')

## Ball x velocity data queue
qvelx = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Ball Xdot Data')

## Platform angle about x-axis data queue
qplatx = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Platform ThetaX Data')

## Platform angle about y-axis data queue
qplaty = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Platform ThetaY Data')

## Platform angular velocity about y-axis data queue
qomegy = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Platform OmegaY Data')

## Platform angular velocity about x-axis data queue
qomegx = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Platform OmegaX Data')

## PWM level for motor 1 data queue
qDC1 = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Motor1 PWM Data')

## PWM level for motor 2 data queue
qDC2 = task_share.Queue('f', 100, thread_protect = True, overwrite = False, name = 'Motor2 PWM Data')

## Time data array
time = array.array('I', [])

## x array
xar = array.array('f', [])

## y array
yar = array.array('f', [])

## z array
zar = array.array('B', [])

## xdot array
xdotar = array.array('f', [])

## ydot array
ydotar = array.array('f', [])

## thx array
thxar = array.array('f', [])

## thy array
thyar = array.array('f', [])

## omx array
omxar = array.array('f', [])

## omy array
omyar = array.array('f', [])

## DC1 array
DC1ar = array.array('f', [])

## DC2 array
DC2ar = array.array('f', [])












