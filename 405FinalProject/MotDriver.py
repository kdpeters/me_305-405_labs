
"""
@file MotDriver.py
@brief This file contains a driver object for a DRV8847 and a "channel" object for single motors

"""
import pyb
import utime
from pyb import Pin

class DRV8847:
    '''
    @brief Driver class to capture the functionality of the DRV8847 motors
    @details Driver includes methods to enable/disable motor, detect fault states,
             and create individual motor channels.
    '''
    def __init__ (self, pnSLEEP, pnFault):
        '''
        @brief Constructor for DRV8847 object
        @param pnSLEEP pyb.Pin object associated with the disabling/enabling the motor system
        @param pnFault pyb.Pin object associated with motor fault detection
        '''
        ## Enable/Disable pin
        self.nSLEEP = pnSLEEP
        self.nSLEEP.init(mode=Pin.OUT_PP)
        self.nSLEEP.low()
        
        ## Fault detection pin
        self.nFault = pnFault
        self.nFault.init(mode=Pin.IN)
        
        ## Fault state boolean
        self.faulty = False
        
        ## External interrupt
        self.extint = pyb.ExtInt(self.nFault, pyb.ExtInt.IRQ_FALLING, Pin.PULL_UP, callback=self.faultCB)
            
        
                
    def enable(self):
        '''
        @brief Enables the motor system
        @details This method does nothing if there is currently an unresolved fault.
                 Temporarily disables further fault interrupts during enabling process.
        '''
        self.extint.disable()
        if(self.faulty):
            pass
        else:
            self.nSLEEP.high()
        utime.sleep_us(10)
        self.extint.enable()
        #print('Motor enabled')
    
    def disable(self):
        '''
        @brief Disables the motor system
        '''
        self.nSLEEP.low()
        #print('Motor disabled')
        
    def faultCB(self, IRQ_Source):
        '''
        @brief Fault interrupt callback method that disables the motor system
        @details Sets the object's fault state to True.
        @param IRQ_Source unused variable from interrupt notation
        '''
        self.disable()
        self.faulty = True
        #print('Something is wrong')
        
    def clearFault(self):
        '''
        @brief Clears fault state for object so that system can be enabled again
        '''
        self.faulty = False
        #print('Good to go')
    
    def channel(self, pIN1, pIN2, timer):
        '''
        @brief Creates a MotorChannel object
        @param pIN1 pyb.Pin object associated with the first input terminal of the motor
        @param pIN2 pyb.Pin object associated with the second input terminal of the motor
        @param timer pyb.Timer object used for PWM signal generation on two input pins
        @return MotorChannel object created from parameterized specifications.
        '''
        return MotorChannel(pIN1, pIN2, timer)


class MotorChannel:
    '''
    @brief Driver subclass to capture the functionality of a single motor
    @details Driver includes a method to set the direction and magnitude of the
             motor shaft rotation.
    '''
    def __init__(self, pIN1, pIN2, timer):
        '''
        @brief Constructor for MotorChannel object
        @param pIN1 pyb.Pin object associated with the first input terminal of the motor
        @param pIN2 pyb.Pin object associated with the second input terminal of the motor
        @param timer pyb.Timer object used for PWM signal generation on two input pins
        '''
        ## Timer object
        self.tim = timer
        
        if(pIN1.name() == 'B4'):
            ## TimerChannel for PWM (IN1)
            self.ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=pIN1)
            ## TimerChannel for PWM (IN2)
            self.ch2 = self.tim.channel(2, pyb.Timer.PWM, pin=pIN2)
        
        elif(pIN1.name() == 'B0'):
            ## TimerChannel for PWM (IN1)
            self.ch1 = self.tim.channel(3, pyb.Timer.PWM, pin=pIN1)
            ## TimerChannel for PWM (IN2)
            self.ch2 = self.tim.channel(4, pyb.Timer.PWM, pin=pIN2)

        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        
    def setDuty(self, duty):
        '''
        @brief Sets the effort outputted by the motor. Direction is tied to input sign
        @param duty Positive or negative integer value between 0 and 100 to set the
               effort and direction of the motor
        '''
        if(duty > 0):
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(duty)
        elif(duty < 0):
            self.ch2.pulse_width_percent(0)
            self.ch1.pulse_width_percent(abs(duty))
        else:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
        
if __name__ == '__main__':
    ## nSleep Pin
    pina15 = Pin(Pin.cpu.A15)
    ## nFault Pin
    pinb2 = Pin(Pin.board.PB2)
    ## Motor 1 IN1
    pinb4 = Pin(Pin.cpu.B4)
    ## Motor 1 IN2
    pinb5 = Pin(Pin.cpu.B5)
    ## Motor 2 IN1
    pinb0 = Pin(Pin.cpu.B0)
    ## Motor 2 IN2
    pinb1 = Pin(Pin.cpu.B1)
    ## Timer for PWM
    timer3 = pyb.Timer(3, freq=20000)
    ## DRV8847 object
    drv = DRV8847(pina15, pinb2)
    ## MotorChannel object for Motor 1
    mot1 = drv.channel(pinb4, pinb5, timer3)
    ## MotorChannel object for Motor 2
    mot2 = drv.channel(pinb0, pinb1, timer3)
    drv.enable()
    
    while True:
        DC = int(input('Enter Motor 1 DC: '))
        if DC>100:
            break
        mot1.setDuty(DC)
    
    while True:
        DC = int(input('Enter Motor 2 DC: '))
        if DC>100:
            break
        mot2.setDuty(DC)
    
    
    
        
        
        
        