""" 
@file main.py
@brief This file simply imports all the tasks and runs the scheduler
"""

import pyb
from micropython import alloc_emergency_exception_buf
import gc

import cotask
import task_share
import TouchTask
import MotTask
import EncTask
import IMUTask
import ControllerTask
import UITask
import DataTask
import Shares

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

# Declare some constants to make state machine code a little more readable.
# This is optional; some programmers prefer to use numbers to identify tasks



# =============================================================================

if __name__ == "__main__":
    print('\'z\' - zero the encoder position')
    print('\'g\' - start balancing')
    print('\'s\' - stop balancing')
    print('\'c\' - clear motor fault')
    print('\'d\' - start data collection')
    print('\'e\' - end data collection')
    try:
        while True:
            cotask.task_list.pri_sched ()
    except KeyboardInterrupt:
        print('Ctrl-C detected.')
        ## Sum of all recorded reaction deltas
        print(cotask.task_list.__repr__())
        print('Length of xar: '+str(len(Shares.xar)))
        print('qballx size: '+str(Shares.qballx.num_in()))
        with open ('StateData.csv', 'w') as file:
            for n in range(len(Shares.xar)):
                file.write('{:},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.2f},{:.2f},{:}\r\n'.format(Shares.time[n],
                                                                                                        1000*Shares.xar[n],
                                                                                                        1000*Shares.xdotar[n],
                                                                                                        Shares.thyar[n],
                                                                                                        Shares.omyar[n],
                                                                                                        1000*Shares.yar[n],
                                                                                                        1000*Shares.ydotar[n],
                                                                                                        Shares.thxar[n],
                                                                                                        Shares.omxar[n],
                                                                                                        Shares.DC1ar[n],
                                                                                                        Shares.DC2ar[n],
                                                                                                        Shares.zar[n]))
        print('CSV file has been created.')


