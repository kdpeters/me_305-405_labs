'''
@file ElevatorTasker.py

@brief This file contains a finite-state-machine for elevator control.

@details There are proximity sensors telling the system what floor the elevator 
         is on and buttons inside the elevator to choose what floor to go to.
'''

from random import choice
import time

class ElevatorTasker:
    '''
    @brief      A finite state machine to control elevator movement between two floors.
    @details    This class implements a finite state machine to control the
                operation of the vertical movement of a simple elevator system.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Elevator moving down
    S1_movdow = 1
    
    ## Constant defining State 2 - Elevator moving up
    S2_movup = 2
    
    ## Constant defining State 3 - Elevator stopped at Floor 1
    S3_stop1 = 3
    
    ## Constant defining State 4 - Elevator stopped at Floor 2
    S4_stop2 = 4
       
    def __init__(self, interval, motor, first, second, Button_1, Button_2, name):
        '''
        @brief      Creates an ElevatorTasker object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## A class attribute "copy" of the Motor object
        self.motor = motor
        
        ## A class attribute "copy" of the Button_1 object
        self.Button_1 = Button_1
        
        ## A class attribute "copy" of the Button_2 object
        self.Button_2 = Button_2
        
        ## The button object used for Floor 1 detection
        self.first = first
        
        ## The button object used for Floor 2 detection
        self.second = second
        
        ## String name for the elevator
        self.name = name
        
        ## Run time interval
        self.interval = interval
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970       
    
        ## The timestamp for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_init):
                print(str(self.runs) + ' ' + str(self.name) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_movdow)
                self.Button_1.setButton(False) 
                self.Button_2.setButton(False)
                self.motor.move(2)
            
            elif(self.state == self.S1_movdow):
                print(str(self.runs) + ' ' + str(self.name) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                self.first.ButtonInput()  #randomizing elevator travel time
                if self.first.getButtonState():
                    self.transitionTo(self.S3_stop1)
                    self.motor.move(0)
                    self.Button_1.setButton(False)
            
            elif(self.state == self.S2_movup):
                print(str(self.runs) + ' ' + str(self.name) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.second.ButtonInput()
                if self.second.getButtonState():
                    self.transitionTo(self.S4_stop2)
                    self.motor.move(0)
                    self.Button_2.setButton(False)
            
            elif(self.state == self.S3_stop1):
                print(str(self.runs) + ' ' + str(self.name) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                self.Button_2.ButtonInput()  #randomizing button pushing of elevator patrons
                if self.Button_2.getButtonState():
                    self.transitionTo(self.S2_movup)
                    self.motor.move(1)
            
            elif(self.state == self.S4_stop2):
                print(str(self.runs) + ' ' + str(self.name) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                self.Button_1.ButtonInput()
                if self.Button_1.getButtonState():
                    self.transitionTo(self.S1_movdow)
                    self.motor.move(2)
                    
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        ## The status of the button
        self.state = False
        
        print('Button object created attached to pin '+ str(self.pin))
        
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @return     A boolean representing the state of the button.
        '''
        return self.state
    
    def setButton(self, val):
        '''
        @brief Sets the button state
        @details Takes the input val and sets it as the button state
        @param val a boolean representing pressed or not pressed
        '''
        self.state = val 
        
    def ButtonInput(self):
        '''
        @brief Simulates a person pressing the button to toggle it
        @return randomly chosen button state
        '''
        self.state = choice([True, False])
        return self.state

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to raise and lower 
                the elevator
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def move(self, cmd):
        '''
        @brief Moves the motor depending on inputted command
        @param cmd int that determines the motion of the motor
        '''
        if cmd == 0:
            print('Elevator STOPPED')
        elif cmd == 1:
            print('Elevator MOVING UP')
        elif cmd == 2:
            print('Elevator MOVING DOWN')
        
    


