'''
@file main_elevator.py
@brief Script to run elevator tasks
'''

from ElevatorTasker import Button, MotorDriver, ElevatorTasker
        
## Motor Object
Motor = MotorDriver()

## Button Object for "First floor button"
Button_1 = Button('PB6')

## Button Object for "Second floor button"
Button_2 = Button('PB7')

## Button Object for "Floor 1 sensor"
FirstSensor = Button('PB8')

## Button Object for "Floor 2 sensor"
SecondSensor = Button('PB9')

## Task object
task1 = ElevatorTasker(0.1, Motor, FirstSensor, SecondSensor, Button_1, Button_2, 'Elevator 1')
## Task object
task2 = ElevatorTasker(0.1, Motor, FirstSensor, SecondSensor, Button_1, Button_2, 'Elevator 2')
# To run the tasks over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()
