import matplotlib.pyplot as plt

def xdd(x):
    return (-2*x-5*(x**3))
    
def xdnext(xdp, xp, xn):
    return (xdp + 0.5*0.01*(xdd(xp) + xdd(xn)))
    
def xnext(xp, xdp):
    xn = xp + xdp*0.01 + 0.5*xdd(xp)*0.01**2
    return xn

def solver(xi, xdi):
    x = [xi]
    xd = [xdi]
    for k in range(999):
        x.append(xnext(x[k], xd[k]))
        xd.append(xdnext(xd[k], x[k], x[k+1]))
    sol = [x, xd]
    return sol

if (__name__ == '__main__'):
    xo = []
    for n in range(6):
        xo.append(.2*n)

    time = []
    for n in range(1000):
        time.append(.01*n)
        
    sols = []
    for n in range(6):
        sols.append((solver(xo[n], 0))[0])

plt.plot(time, sols[0], label='Xo = 0')
plt.plot(time, sols[1], label='Xo = 0.2')
plt.plot(time, sols[2], label='Xo = 0.4')
plt.plot(time, sols[3], label='Xo = 0.6')
plt.plot(time, sols[4], label='Xo = 0.8')
plt.plot(time, sols[5], label='Xo = 1.0')
plt.xlabel('Time [seconds]')
plt.ylabel('x(t) [units]')
plt.legend()
