
"""
@file ExternalTemp.py
@brief This file contains a class representing a temperature sensor and brief testing code
"""

from pyb import I2C
import utime

class mcp9808:
    '''
    @brief Wrapper class for basic functionality of the Adafruit MCP9808 temperature sensor
    @details Methods include an ID register check, celsius temperature acquisition,
             and fahrenheit temperature acquisition
    '''
    
    def __init__(self, i2c, address):
        '''
        @brief Initializes the mcp9808 object with an I2C object and device address
        @param i2c Master I2C object for control of the physical temperature sensor
        @param address Physical temperature sensor device address 
        '''
        ## I2C object class copy
        self.myi2c = i2c
        ## Device address
        self.slavead = address
        ## Read data array from a device memory register
        self.readdata = bytearray([0, 0])
    
    def check(self):
        '''
        @brief Checks the device's manufacturer ID register
        @return Temperature sensor ID register in hexadecimal
        '''
        self.myi2c.mem_read(self.readdata, self.slavead, 6)
        # regID should be 0x0054
        regID1 = str(self.readdata[0])
        regID2 = int(regID1+str(self.readdata[1]))
        regID3 = hex(regID2)
        return regID3
    
    def celsius(self):
        '''
        @brief Retrieves ambient temperature data from the sensor
        @return Ambient temperature in degrees celsius
        '''
        self.myi2c.mem_read(self.readdata, self.slavead, 5)
        upperbyte = self.readdata[0]
        lowerbyte = self.readdata[1]
        upperbyte = upperbyte & 0x1f
        if (upperbyte & 0x10)==0x10:
            upperbyte = upperbyte & 0x0f
            temp = 256-(16*upperbyte+lowerbyte/16)
        else:
            temp = (16*upperbyte+lowerbyte/16)
        return temp
    
    def fahrenheit(self):
        '''
        @brief Retrieves ambient temperature data from the sensor
        @return Ambient temperature in degrees fahrenheit
        '''
        ctemp = self.celsius()
        ftemp = 1.8*ctemp + 32
        return ftemp


if __name__=='__main__':
    ## I2C object
    i2c = I2C(1)
    i2c.init(I2C.MASTER)
    ## mcp9808 object
    sensor = mcp9808(i2c, 0x18)
    
    print(sensor.check())
    ## List of temperature data
    temps = []
    for n in range(60):
        temps.append(sensor.celsius())
        utime.sleep(1)
    print(temps)
    
    
