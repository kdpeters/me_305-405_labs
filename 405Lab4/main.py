"""
@file main.py
@brief This file facilitates STM32 internal temperature and ambient temperature recording.
@details A simple finite state machine implementing internal and external temperature
         scripts runs continuously until the user stops data collection. A .csv
         file of the data along with time stamps is then generated.
"""
from pyb import I2C
import utime
import InternalTemp
import ExternalTemp
import array

## I2C object
i2c = I2C(1)
i2c.init(I2C.MASTER)
## mcp9808 object
sensor = ExternalTemp.mcp9808(i2c, 0x18)
## List of internal temperature data
intdata = array.array('f', [])
## List of external temeprature data
extdata = array.array('f', [])
## List of time stamps
tdata = bytearray([])

def runTempCollect():
    '''
    @brief Finite state machine to collect temperature data and generate time stamps
    '''
    global intdata
    global extdata
    global sensor
    global tdata
    state = 0
    time = 0
    while True:
        if state==0:
            intdata.append(InternalTemp.getIntTemp())
            extdata.append(sensor.celsius())
            tdata.append(time)
            time+=1
        else:
            pass
        yield(state)

if __name__=='__main__':
    ## Generator object
    gage = runTempCollect()
    
    try:
        while True:
            next(gage)
            utime.sleep(60)
            
    except KeyboardInterrupt:
        print('Ctrl-C detected.')
        ## CSV file of data
        with open ('TempData.csv', 'w') as file:
            for n in range(len(intdata)):
                file.write('{:.1f},{:.2f},{:.2f}\r\n'.format(tdata[n], intdata[n], extdata[n]))
        print('CSV file has been created.')
        
        
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')




