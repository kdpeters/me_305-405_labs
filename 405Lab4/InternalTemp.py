
"""
@file InternalTemp.py
@brief This file contains a simple method for recording internal microcontroller temperature.
@details This file also contains practice code for writing data to a .csv file.

"""
import pyb
import utime

def getIntTemp():
    '''
    @brief Uses the ADCAll module to obtain the internal STM32 temperature in celsius
    @return float representing celsius temperature
    '''
    adcall = pyb.ADCAll(12, 0x70000)
    adcall.read_core_vref()
    return adcall.read_core_temp()

if __name__=='__main__':
    ## List of temperature data taken every second for a minute
    temps = []
    for n in range(60):
        temps.append(getIntTemp())
        utime.sleep(1)
    ## Writing test file
    with open ('testCSV.csv', 'w') as file:
        for temperature in temps:
            file.write('{:}\r\n'.format(temperature))

