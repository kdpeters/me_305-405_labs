"""
@file ReactionTest.py
@brief Reaction time to an LED flash is measured via a micropython timing module and an external interrupt.
@details The utime module is used to manage the timing of the LED flash and reaction
         time measurement. The average reaction time is calculated from all the 
         trials the user participates in and is displayed once Ctrl-C is entered. 
         A message is displayed if the user never presses the button.
"""
import pyb
from pyb import Pin
import utime
import random

## List of reactions times
rtimes = []

## Button pin setup
but = Pin(Pin.cpu.C13)

## LED start time
LEDONtime = 0

## Current reaction time
diff = 0


def isr (but):
    '''
    @brief External interrupt callback that records the time of the button press and computes the reaction time.
    @param but Pin corresponding to the button being pressed.
    '''
    global LEDONtime
    global diff
    if LEDONtime==0:
        pass
    else:
        presstime = utime.ticks_us()
        diff = utime.ticks_diff(presstime, LEDONtime)

## External interrupt
extint = pyb.ExtInt(but, pyb.ExtInt.IRQ_FALLING, Pin.PULL_UP, callback=isr)

def runReacTime():
    '''
    @brief Generator that continually checks to see if a reaction time has been recorded.
    @details If a reaction delta count has been recorded, it is added to the list
             of reaction deltas. The states of the generator operate the LED timing.
    '''
    global LEDONtime
    global diff
    ## LED pin
    p5 = Pin(Pin.cpu.A5, mode=Pin.OUT_PP)
    p5.low()
    
    ## int representing current state of program
    state = 0
    
    ## trial start time
    trialstart = 0
    
    ## delay time before LED
    delay = 0
    
    while True:
        if state==0:
            if diff>0:
                rtimes.append(diff)
                diff = 0
                LEDONtime = 0
            
            trialstart = utime.ticks_us()
            delay = utime.ticks_add(trialstart, int(2e6))
            delay = utime.ticks_add(delay, int(random.randint(0, int(1e6))))
            state = 1
            
        elif state==1:
            if diff>0:
                rtimes.append(diff)
                diff = 0
                LEDONtime = 0
                
            currtime = utime.ticks_us()
            if utime.ticks_diff(currtime, delay)>0:
                p5.high()
                LEDONtime = utime.ticks_us()
                LEDOFFtime = utime.ticks_add(LEDONtime, int(1e6))
                state = 2
                
        elif state==2:
            if diff>0:
                rtimes.append(diff)
                diff = 0
                LEDONtime = 0
                
            currtime = utime.ticks_us()
            if utime.ticks_diff(currtime, LEDOFFtime)>0:
                p5.low()
                state = 0
                
                    
        else:
            # this state should not exist
            pass
        yield (state)
    


        
     
if __name__=='__main__':
    ## Generator setup
    reac = runReacTime()
    
    try:
        while True:
            next(reac)
            utime.sleep_us(10)
            
    except KeyboardInterrupt:
        print('Ctrl-C detected.')
        ## Sum of all recorded reaction deltas
        summ = 0
        if len(rtimes)==0:
            print('You did not press the button!')
        else:
            for n in range(len(rtimes)):
                summ += rtimes[n]
            ## Average reaction time in seconds
            avg = summ/(len(rtimes)*(1e6))
            print('Your average reaction time was '+str(avg)+' seconds!')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
        



