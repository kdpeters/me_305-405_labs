"""
@file ReactTimer.py
@brief Reaction time to an LED flash is measured via a Nucleo timer and OC and IC interrupts.
@details An output compare timer channel is used to operate the LED flashes and 
         an input capture timer channel is used to record timer count when a user
         button is pushed in response to the LED flash. The average reaction time
         is calculated from all the trials the user participates in and is displayed
         once Ctrl-C is entered. A message is displayed if the user never presses
         the button.
"""
import pyb
import micropython
from pyb import Pin
from pyb import Timer
import random
import utime

micropython.alloc_emergency_exception_buf(100)

## Timer period
PER = int((0xffffffff)/64)

## Timer prescaler
PS = 8

## delta t (12.5 ns * PS) in microseconds between timer counts
dt = 100

## time period per overflow in seconds
T = 6.710

## LED pin setup
p5 = Pin(Pin.cpu.A5, mode=Pin.OUT_PP)
p5.value(1)

## IC pin setup (button is connected here via jumper on Nucleo)
b3 = Pin(Pin.cpu.B3)

## List of reactions times
rtimes = []

## Last OC compare value
lastcomp = 0

## Timer count at LED activation
LEDONtime = 0

## Delta count for reaction
diff = 0

## Boolean LED on or off
LED = False

def OCcall(Tim2):
    '''
    @brief Output compare callback method that updates when the next LED toggling happens.
    @details The callback flashes the LED for one second then waits 2-3 seconds 
             before flashing again for the next reaction test.
    @param Tim2 Timer object that counts for the OC channel
    '''
    global lastcomp
    global LED
    global LEDONtime
    global PER
    lastcomp = t2ch1.compare()
    if LED==False:
        LEDONtime = lastcomp
        LED = True
        if (lastcomp + int(1e7))>PER:
            lastcomp = lastcomp+int(1e7)-PER
        else:
            lastcomp += int(1e7)
    else:
        LED = False
        if (lastcomp + int(2e7)+int(random.randint(0, int(1e7))))>PER:
            lastcomp = lastcomp+int(2e7)+int(random.randint(0, int(1e7)))-PER
        else:
            lastcomp += int(2e7)+int(random.randint(0, int(1e7)))
    t2ch1.compare(lastcomp)

def ICcall(Tim2):
    '''
    @brief Input capture callback method that records timer count when user button is pressed.
    @param Tim2 Timer object that counts for the IC channel
    '''
    global diff
    global LEDONtime
    if LEDONtime==0:
        pass
    else:
        presstime = t2ch2.capture()
        diff = presstime-LEDONtime
        

## Timer setup
Tim2 = Timer(2, prescaler=PS, period=PER)

## OC channel
t2ch1 = Tim2.channel(1, mode=Timer.OC_TOGGLE, pin=p5, compare=int(2e7), callback=OCcall)

## IC channel
t2ch2 = Tim2.channel(2, mode=Timer.IC, pin=b3, polarity=Timer.FALLING, callback=ICcall)




def runReacTime():
    '''
    @brief Generator that continually checks to see if a reaction time has been recorded.
    @details If a reaction delta count has been recorded, it is added to the list
             of reaction deltas.
    '''
    global diff
    global LEDONtime

    ## int representing current state of program
    state = 0
    
    while True:
        if state==0:
            if diff!=0:
                rtimes.append(diff)
                diff = 0
                LEDONtime = 0

        else:
            # this state should not exist
            pass
        yield (state)
    
if __name__=='__main__':
    ## generator setup
    reac = runReacTime()
    
    try:
        while True:
            next(reac)
            utime.sleep(0.001)
            
    except KeyboardInterrupt:
        print('Ctrl-C detected.')
        ## Sum of all recorded reaction deltas
        summ = 0
        if len(rtimes)==0:
            print('You did not press the button!')
        else:
            for n in range(len(rtimes)):
                summ += rtimes[n]
            ## Average reaction time in seconds
            avg = summ/(len(rtimes)*(1e7))
            print('Your average reaction time was '+str(avg)+' seconds!')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
        





