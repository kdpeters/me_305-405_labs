
'''
@file FibonacciCalculator.py
@brief  Contains the fibonnacci() function and a main to test it.
 
@details  fibonacci() function calculates the Fibonacci number at the 
          inputted index.

@author Kyle Peterson

@copyright License Info

@date September 22, 2020
'''


def fibonacci (idx):
    ''' 
    @brief  Determines the number in the Fibonacci sequence at specified index.

    @details  Uses a list that is generated each function call to store previously 
              calculated fibonacci numbers.

    "fiblist" is the compiled list of calcualted Fibonacci numbers that the function
    calls upon using memoisation.

    "intidx" is the initial index for the loop that runs until the user index is reached.

    "newfibnum" represents the newest fibonacci number calculated that is to be added
    to the end of fiblist each cycle through the loop.

    @param idx An int index indicating the Fibonacci number to calculate and display. 
    '''
    fiblist = [0, 1]
    intidx = 1
    newfibnum = 0
    print('Calculating Fibonacci number at index n = {:}.'.format(useridx))
    if (idx == 0) | (idx == 1):
        print(idx)
    while intidx < idx:
        newfibnum = fiblist[-1] + fiblist[-2]
        fiblist.append(newfibnum)
        intidx = intidx + 1
    if (idx != 0) and (idx != 1):
        print(fiblist[-1])
if __name__ == '__main__':
    ## Initial value before user index input in main
    useridx = -1
    while useridx < 0:
        useridx = 'yay'
        while type(useridx) != int:
            try:
                useridx = int(input('Please enter a valid index: '))
            except ValueError:
                print('Error! Please enter a valid index.')
    fibonacci(useridx)

 
        