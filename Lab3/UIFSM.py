
"""
@file UIFSM.py
@brief This file contains a finite-state-machine for a user interface.

@details Continuously checks for user input commands.
"""
import utime
import EncGlobals
from pyb import UART


class UserInterface:
    '''
    @brief      A finite state machine to run a basic user interface with an encoder
    
    @details    This class implements a finite state machine to read user input
                data to control and interpret an encoder. 'z' zeros the position,
                'p' prints the position, and 'd' prints the latest delta
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Checking and taking action
    S1_check = 1
       
    def __init__(self):
        '''
        @brief      Creates a UIFSM object
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Class attribute uart object
        self.uint = UART(2)
        
        ## Running task time interval in microseconds
        self.interval = int(0.1*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Int representing the latest user input command
        self.cmd = 0
        
        
        print('\nCommands at your disposal:\n\'z\' - zero the encoder')
        print('\'p\' - print the encoder position\n\'d\' - print the encoder delta\n')
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                # Run State 0 Code
                self.transitionTo(self.S1_check)
            
            elif(self.state == self.S1_check):
                # Run State 1 Code
                if(self.uint.any()):
                    self.cmd = self.uint.readchar()
                    if(self.cmd == 122):
                        EncGlobals.reset = True
                    elif(self.cmd == 112):
                        print(str(EncGlobals.position))
                    elif(self.cmd == 100):
                        print(str(EncGlobals.delta))
                self.transitionTo(self.S1_check)

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState





