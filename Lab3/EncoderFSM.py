
"""
@file EncoderFSM.py
@brief This file contains a finite-state-machine for encoder reading.

@details Continuously updates the position of the motor of an Encoder object.
"""
import utime
import pyb
import EncGlobals
import Encoder


class EncoderFSM:
    '''
    @brief      A finite state machine to run position updates of a 16-bit encoder
    
    @details    This class implements a finite state machine to track net
                position of an encoder's motor at an appropriate time interval
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Updating
    S1_up = 1
    
    ## Period of a 16-bit counter
    Period = 0xFFFF
       
    def __init__(self, encoder, cpr, maxrpm):
        '''
        @brief      Creates an EncoderFSM object
        @param encoder EncoderDriver object
        @param cpr int number of pole pairs in the encoder
        @param maxrpm int maximum rpm experienced by the encoder
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Class attribute "copy" of the Encoder object
        self.encoder = encoder
        
        ## Running task time interval in microseconds
        self.interval = int((4*cpr*maxrpm/60)/(0.5*self.Period)*1e6)
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                # Run State 0 Code
                self.transitionTo(self.S1_up)
            
            elif(self.state == self.S1_up):
                # Run State 1 Code
                if(EncGlobals.reset):
                    self.encoder.set_position(0)
                    EncGlobals.reset = False
                self.encoder.update()
                EncGlobals.position = self.encoder.get_position()
                EncGlobals.delta = self.encoder.get_delta()
                self.transitionTo(self.S1_up)

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState



