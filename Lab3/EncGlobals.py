'''
@file EncGlobals.py
@brief Script to define and store global variables to be shared between FSMs
'''
## Net position of the encoder
position = 0

## Delta position between last two update calls of the encoder
delta = 0

## Position reset request by user
reset = False