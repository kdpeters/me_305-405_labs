'''
@file main_Enc.py
@brief Script to run an encoder and basic user interface
'''

import Encoder
from EncoderFSM import EncoderFSM
import pyb
from UIFSM import UserInterface

## Encoder object for encoder finite state machine
encoder = Encoder.EncoderDriver(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3)

## Encoder finite state machine
task1 = EncoderFSM(encoder, 7, 10)

## UI finite state machine
task2 = UserInterface()

while True:
    task1.run()
    task2.run()
    
