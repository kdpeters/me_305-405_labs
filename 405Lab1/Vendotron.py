"""
@file Vendotron.py 
@brief Vending machine finite state machine and state generator
@details Contains the callback function to enable kayboard use as well as the 
         Vendotron generator.

"""
import time
from time import sleep
from getChange import getChange

import keyboard 

def kb_cb(key):
    """ 
    @brief Callback function which is called when a key has been pressed.
    @param key object representing key that has been pressed on the keyboard.
    """
    global lk
    lk = key.name

def VendotronTask():
    """ 
    @brief Runs one iteration of the vending machine task.
    @details Contains states for idling, dispensing liquids, and dispensing solids. 
             If there're insufficient funds when a beverage is selected, its price
             is displayed. Balance is updated as money is inputed. Efficient change
             dispensed when requested.
    """
    ## int representing current state of Vendotron
    state = 0
    ## int representing current balance in Vendotron
    Bal = 0
    ## dictionary representing denominations of currency in Vendotron
    Baldenom = {'pen':0, 'nic':0, 'dim':0, 'qua':0, 'one':0, 'fiv':0, 'ten':0, 'twe':0}
    ## float representing time in seconds since boot up/last operation
    currt = 0
    
    global lk
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print('\nVendotron is hungry for cash!\nBal: '+'{:.2f}'.format(Bal/100))
            currt = time.time()
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            # perform state 1 operations
            if lk == '0':
                Bal += 1
                Baldenom['pen'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '1':
                Bal += 5
                Baldenom['nic'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '2':
                Bal += 10
                Baldenom['dim'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '3':
                Bal += 25
                Baldenom['qua'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '4':
                Bal += 100
                Baldenom['one'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '5':
                Bal += 500
                Baldenom['fiv'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '6':
                Bal += 1000
                Baldenom['ten'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == '7':
                Bal += 2000
                Baldenom['twe'] += 1
                lk = ''
                currt = time.time()
                print('\nMake a selection\nBal: '+'{:.2f}'.format(Bal/100))
            elif lk == 'E':
                state = 2
                lk = ''
            elif lk == 'C':
                state = 3
            elif lk == 'D':
                state = 3
            elif lk == 'S':
                state = 3
            elif lk == 'P':
                state = 3
            elif (time.time() - currt) > 60:
                currt = time.time()
                print('\nTry Cuke NOW!!!!\nBal: '+'{:.2f}'.format(Bal/100))
            
            
        elif state == 2:
            change = getChange(0, Baldenom)
            print('\nYour change: '+'{:.2f}'.format(Bal/100))
            print('pennies:  '+str(change['pen']))
            print('nickels:  '+str(change['nic']))
            print('dimes:    '+str(change['dim']))
            print('quarters: '+str(change['qua']))
            print('ones:     '+str(change['one']))
            print('fives:    '+str(change['fiv']))
            print('tens:     '+str(change['ten']))
            print('twenties: '+str(change['twe']))
            Bal = 0
            print('\nTake your change...or leave it for Vendotron!\nBal: '+'{:.2f}'.format(Bal/100))
            currt = time.time()
            state = 1
            
        elif state == 3:
            if lk == 'C':
                lk = ''
                price = 100
                change = getChange(price, Baldenom)
                if change == None:
                    print('\nInsufficient funds!\nCuke price: '+'{:.2f}'.format(price/100))
                    state = 1
                else:
                    Bal -= price
                    Baldenom = change
                    print('\nEnjoy your Cuke, but also try Popsy!\nBal: '+'{:.2f}'.format(Bal/100))
                    state = 1
                currt = time.time()
            elif lk == 'D':
                lk = ''
                price = 110
                change = getChange(price, Baldenom)
                if change == None:
                    print('\nInsufficient funds!\nDr. Pupper price: '+'{:.2f}'.format(price/100))
                    state = 1
                else:
                    Bal -= price
                    Baldenom = change
                    print('\nEnjoy your Dr. Pupper, but also try Cuke!\nBal: '+'{:.2f}'.format(Bal/100))
                    state = 1
                currt = time.time()
            elif lk == 'S':
                lk = ''
                price = 85
                change = getChange(price, Baldenom)
                if change == None:
                    print('\nInsufficient funds!\nSpryte price: '+'{:.2f}'.format(price/100))
                    state = 1
                else:
                    Bal -= price
                    Baldenom = change
                    print('\nEnjoy your Spryte, but also try Dr. Pupper!\nBal: '+'{:.2f}'.format(Bal/100))
                    state = 1
                currt = time.time()
            elif lk == 'P':
                lk = ''
                price = 120
                change = getChange(price, Baldenom)
                if change == None:
                    print('\nInsufficient funds!\nPopsy price: '+'{:.2f}'.format(price/100))
                    state = 1
                else:
                    Bal -= price
                    Baldenom = change
                    print('\nEnjoy your Popsy, but also try Spryte!\nBal: '+'{:.2f}'.format(Bal/100))
                    state = 1
                currt = time.time()
            
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)



if __name__ == "__main__":
    ## Empty initial keyboard entry
    lk = ''
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("E", callback=kb_cb)
    keyboard.on_release_key("C", callback=kb_cb)
    keyboard.on_release_key("P", callback=kb_cb)
    keyboard.on_release_key("S", callback=kb_cb)
    keyboard.on_release_key("D", callback=kb_cb)
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("6", callback=kb_cb)
    keyboard.on_release_key("7", callback=kb_cb)

    ## Vendotron generator object set up 
    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')