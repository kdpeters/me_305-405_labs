
"""
@file getChange.py
@brief This file contains a function to compute change returned by a vending machine.
"""

def getChange(price, payment):
    '''
    @brief function to compute change returned given a payment and price with the fewest denominations.
    @param price int representing the price to be paid in cents.
    @param payment dictionary containing number of each denomination in payment. 
           Dictionary keys are the first three letters of each denomination ranging
           from pennies to twenties.
    @return dictionary similar to payment containing number of each denomination 
            in correct change amount. Returns None if payment is not sufficient.
    '''
    change = {'pen':0, 'nic':0, 'dim':0, 'qua':0, 'one':0, 'fiv':0, 'ten':0, 'twe':0}
    pay = payment['pen']+5*payment['nic']+10*payment['dim']+25*payment['qua']+100*payment['one']+500*payment['fiv']+1000*payment['ten']+2000*payment['twe']
    changetotal = pay-price
    if changetotal < 0:
        return None
    change['twe'] = changetotal//2000
    CT1 = changetotal%2000
    change['ten'] = CT1//1000
    CT2 = CT1%1000
    change['fiv'] = CT2//500
    CT3 = CT2%500
    change['one'] = CT3//100
    CT4 = CT3%100
    change['qua'] = CT4//25
    CT5 = CT4%25
    change['dim'] = CT5//10
    CT6 = CT5%10
    change['nic'] = CT6//5
    CT7 = CT6%5
    change['pen'] = CT7
    
    return change

if __name__ == "__main__":
    ## $31.02 payment
    payment = {'pen':2, 'nic':5, 'dim':0, 'qua':3, 'one':0, 'fiv':0, 'ten':1, 'twe':1}
    ## $15.35 price
    price = 1535
    ## Change should be $15.67
    # 1 ten
    # 1 five
    # 2 quarters
    # 1 dime
    # 1 nickel
    # 2 pennies
    change = getChange(price, payment)
    print(change)

    
