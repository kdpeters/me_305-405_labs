
"""
Created on Thu Apr  8 12:11:21 2021

@author: kdpet
"""

def fibSeq(maxnum):
    #find first number
    f0 = 0
    yield f0
    #find second number
    f1 = 1
    yield f1
    #find general number
    count = 0
    while count<maxnum:
        count += 1
        f2 = f1+f0
        f0 = f1
        f1 = f2
        yield f2
    
if __name__ == "__main__":
    mySeq = fibSeq(15)
    # print(next(mySeq))
    # print(next(mySeq))
    # print(next(mySeq))
    # print(next(mySeq))
    # print(next(mySeq))
    for fibNum in mySeq:
        print(fibNum)