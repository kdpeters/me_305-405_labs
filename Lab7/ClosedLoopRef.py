
"""
@file ClosedLoopRef.py

@brief This file contains a ClosedLoop object that computes motor level

@details Uses proportional control and a provided gain to determine appropriate
         motor effort levels.

"""

class ClosedLoop:
    '''
    @brief Computes the motor effort level to control the reference profile response 
    
    @details Methods include updating motor level as well as getting and setting
             the gain value.
    '''
    def __init__(self, kpi, kplimit):
        '''
        @brief Creates a ClosedLoop object
        @param kpi float defining the initial Kp value
        @param kplimit float defining a limit on the Kp value
        '''
        ## Gain value
        self.kp = kpi
        
        ## Kp limit to keep value reasonable
        self.kplim = kplimit
        
        if(self.kp > self.kplim):
            self.kp = self.kplim
    
    def update(self, omegacurr, omegaref):
        '''
        @brief Computes the motor effor level according to the gain and current error
        @param omegacurr float representing the current motor speed
        @param omegaref float representing the reference motor speed
        @return int representing the computed effort level in percent
        '''
        level = (self.kp/3.3)*(omegaref - omegacurr)
        if level > 100.0:
            level = 100
        return int(level)
    
    def getKp(self):
        '''
        @brief gets the current gain value
        @return float representing the current gain value
        '''
        return self.kp
    
    def setKp(self, newkp):
        '''
        @brief sets the gain to the new value
        @param newkp float defining the new gain value
        '''
        if(newkp > self.kplim):
            self.kp = self.kplim
        else:
            self.kp = newkp
        
    
    
    
    