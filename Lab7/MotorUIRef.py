
"""
@file MotorUIRef.py

@brief This file serves as the user interface for motor data collection on the Nucleo
@details Uses the UART module to take user input to the Nucleo and return motor data 
         back to the computer.
"""

import utime
import MotorSharesRef
from pyb import UART
import array



class MotorUser:
    '''
    @brief A finite state machine to run a basic user interface with a data collection FSM
    
    @details    This class implements a finite state machine to read user input
                to control a data collection object. It sends finished data
                series back to the computer. It reads a sent Kp value and uses 
                it to generate the motor response. The class sends data back after
                15 seconds.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Checking and taking action
    S1_check = 1
    
    ## Constant defining State 2 - Receiving data
    S2_collect = 2
    
    def __init__(self):
        '''
        @brief Creates a MotorUser object
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
        
        ## Class attribute uart object
        self.uint = UART(2)
        
        ## Running task time interval in microseconds
        self.interval = int(0.005*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Run counter
        self.runs = 0
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                self.transitionTo(self.S1_check)
            
            elif(self.state == self.S1_check):
                if(self.uint.any() > 0):
                    rline = self.uint.readline().decode('ascii').strip()
                    MotorSharesRef.kp = float(rline)
                    self.transitionTo(self.S2_collect)
                    self.runs = 0
                    
            elif(self.state == self.S2_collect):
                if(self.runs > 3100):
                    for n in range(len(MotorSharesRef.vArray)):
                        data = array.array('f', [])
                        data.append(MotorSharesRef.vArray[n])
                        data.append(MotorSharesRef.pArray[n])
                        self.uint.write('{:},{:}'.format(data[0], data[1]) +'\r\n')
                    self.transitionTo(self.S1_check)
                self.runs += 1

            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState int representing the next state to transition to
        '''
        self.state = newState