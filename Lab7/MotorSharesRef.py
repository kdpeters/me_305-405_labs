'''
@file MotorSharesRef.py
@brief Script to define and store global variables to be shared between FSMs
'''
## Current user input gain
kp = 0

## Data array for omega actual collection
vArray = None

## Data array for actual position collection
pArray = None

## Data array for reference omega 
rvArray = None

## Data array for reference position
rpArray = None
