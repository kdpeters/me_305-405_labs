'''
@file main.py
@brief Script to run motor control and motor response data collection
'''

from Encoder import EncoderDriver
from MotorDriver import MotoDrive
import pyb
from MotorUIRef import MotorUser
from MotorCtrlRef import Controller
from ClosedLoopRef import ClosedLoop
import array
import MotorSharesRef

## Time array from reference file
time = array.array('f', [])

## Velocity array from reference file
velocity = array.array('f', [])

## Position array from reference file
position = array.array('f', [])

## Counter for parsing necessary data in reference file
counter = 0

# Open the file. From here it is essentially identical to serial comms. There
# are better ways to do this, like using the 'with' statement, but using an
# object like this is closest to the serial code you've already used.
## Reader for CSV reference profile data
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.
#
# You will need to handle this slightly differently on the Nucleo or
# pre-process the provided data, because there are 15,001 rows of data in the
# CSV which will be too much to store in RAM on the Nucleo.
#
# Consider resampling the data, interpolating the data, or manipulating the 
# data in some fashion to limit the number of rows based on the 'interval' for
# your control task. If your controller runs every 20ms you do not need the
# data sampled at 1ms provided by the file.
#
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    ## Current line being read in CSV file
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    elif(counter%20 == 0):
        ## time, velocity, and position data from CSV line
        (t,v,x) = line.strip().split(',');
        time.append(float(t))
        velocity.append(20*float(v))
        position.append(20*float(x))

    counter += 1
# Can't forget to close the serial port
ref.close()

## Setting velocity reference profile
MotorSharesRef.rvArray = velocity

## Setting position reference profile
MotorSharesRef.rpArray = position

## Encoder object for Controller FSM
encoder = EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

## MotoDrive object for Controller FSM
motodrive = MotoDrive(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, pyb.Timer(3, freq=20000))

## ClosedLoop object for Controller FSM
closedloop = ClosedLoop(0.001, 1.0)

## MotorUser FSM
task1 = MotorUser()

## Controller FSM
task2 = Controller(encoder, motodrive, closedloop)

while True:
    task1.run()
    task2.run()
    

