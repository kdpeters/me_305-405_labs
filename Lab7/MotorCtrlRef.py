
"""
@file MotorCtrlRef.py

@brief This file serves as the FSM to run a reference profile motor response
@details Uses the Encoder, MotoDrive, and ClosedLoop classes to obtain angular 
         velocity and position data on the Nucleo and store it for transmission 
         to the computer.
"""

import utime
import MotorSharesRef
import array

class Controller:
    '''
    @brief      A finite state machine to run data collect of motor response
    
    @details    This class implements a finite state machine to record data during 
                a 15-second time period and coordinate the proportional control of 
                the motor.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_init = 0
    
    ## Constant defining State 1 - Data Acquisition
    S1_datacq = 1
       
    def __init__(self, encoder, motodrive, closedloop):
        '''
        @brief Creates a Controller object
        @param encoder EncoderDriver object to get motor position data
        @param motodrive MotoDrive object to interact with the motor hardware
        @param closedloop ClosedLoop object for computing the level to send to the motor
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_init
           
        ## Running task time interval in microseconds
        self.interval = int(0.02*(1e6))
       
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()       
    
        ## The timestamp for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Data array for omega actual collection
        self.VDarray = None
        
        ## Data array for actual position collection
        self.PDarray = None
        
        ## Class attribute EncoderDriver object
        self.enc = encoder
        
        ## Class attribute MotoDrive object
        self.md = motodrive
        
        ## Class attribute ClosedLoop object
        self.clp = closedloop
        
        ## Run Counter
        self.runs = 0
    
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Current time of function call
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) > 0):
            if(self.state == self.S0_init):
                if(MotorSharesRef.kp != 0):
                    self.VDarray = array.array('f', [])
                    self.PDarray = array.array('f', [])
                    self.runs = 0
                    self.enc.set_position(0)
                    self.md.enable()
                    self.md.setDuty(0)
                    self.transitionTo(self.S1_datacq)
            
            elif(self.state == self.S1_datacq):
                if(self.runs < 751):
                    self.clp.setKp(MotorSharesRef.kp)
                    self.enc.update()
                    drevs = (self.enc.get_delta()/4000)
                    omegacurr = 60*(drevs/(self.interval/(1e6)))
                    posit = self.enc.get_position()
                    dposit = 360*(posit/4000)
                    level = self.clp.update(omegacurr, MotorSharesRef.rvArray[self.runs])
                   
                    self.md.setDuty(-1*level)
                    self.VDarray.append(omegacurr)
                    self.PDarray.append(dposit)
                else:
                    MotorSharesRef.vArray = self.VDarray
                    MotorSharesRef.pArray = self.PDarray
                    self.md.setDuty(0)
                    self.md.disable()
                    self.enc.set_position(0)
                    MotorSharesRef.kp = 0
                    self.transitionTo(self.S0_init)
                self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) 
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState int representing the next state to transition to
        '''
        self.state = newState

