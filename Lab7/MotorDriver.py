
"""
@file MotorDriver.py

@brief This file contains a driver object for a DC motor

@details Driver incorporates methods to enable/disable motor and set its effort
         and direction
"""
import pyb

class MotoDrive:
    '''
    @brief Uses PWM signals from specified Nucleo pins to control the effort of the motor
    
    @details Driver incorporates methods to enable/disable motor and set its effort
             and direction
    '''
    def __init__ (self, pnSLEEP, pIN1, pIN2, timer):
        '''
        @brief Creates a MotoDrive object
        @param pnSLEEP enable/disable pin - active high
        @param pIN1 PWM pin for controlling half of the H-bridge
        @param pIN2 PWM pin for controlling other half of the H-bridge
        @param timer timer object used for PWM generation for IN1 and IN2
        '''
        ## Enable/Disable pin
        self.nSLEEP = pyb.Pin(pnSLEEP, pyb.Pin.OUT_PP)
        
        ## timer for PWMs
        self.tim = timer
        
        ## First PWM channel
        self.ch1 = None
        
        ## Second PWM channel
        self.ch2 = None
        
        if(pIN1.name() == 'B4'):
            self.ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=pIN1)
            
            self.ch2 = self.tim.channel(2, pyb.Timer.PWM, pin=pIN2)
        
        if(pIN1.name() == 'B0'):
            self.ch1 = self.tim.channel(3, pyb.Timer.PWM, pin=pIN1)
            
            self.ch2 = self.tim.channel(4, pyb.Timer.PWM, pin=pIN2)

        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
                
    def enable(self):
        '''
        @brief Enables the motor
        '''
        self.nSLEEP.high()
        #print('Motor enabled')
    
    def disable(self):
        '''
        @brief Disables the motor
        '''
        self.nSLEEP.low()
        #print('Motor disabled')
        
    def setDuty(self, duty):
        '''
        @brief Sets the effort outputted by the motor. Direction is tied to input sign
        @param duty Positive or negative integer value between 0 and 100 to set the
               effort and direction of the motor
        '''
        if(duty > 0):
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(duty)
        elif(duty < 0):
            self.ch2.pulse_width_percent(0)
            self.ch1.pulse_width_percent(abs(duty))
        else:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
        
# if __name__ == '__main__':
#     pina15 = pyb.Pin(pyb.Pin.cpu.A15)
#     pinb4 = pyb.Pin(pyb.Pin.cpu.B4)
#     pinb5 = pyb.Pin(pyb.Pin.cpu.B5)
#     timer3 = pyb.Timer(3, freq=20000)
#     moe = MotoDrive(pina15, pinb4, pinb5, timer3)
#     moe.enable()
#     moe.setDuty(0)
        
        
        
        